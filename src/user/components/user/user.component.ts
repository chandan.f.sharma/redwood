import { Component, OnInit, OnDestroy} from '@angular/core';

import { Store, select } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { User } from '../../models/user.model';
import * as UserSelectors from '../../store/user.selector';
import * as UserActions from '../../store/user.action';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {

  userState: Observable<User>;
  user: User;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private store: Store<User>) {}

  ngOnInit() {

    // this.store
    // .select<any>((state: any) => state) // the complete state this time!!!
    // .subscribe((completeState: any) => console.log('========>>>',completeState));

    this.userState = this.store.pipe(select(UserSelectors.selectUser));
    console.log(this.userState);
    this.userState.pipe(takeUntil(this.ngUnsubscribe)).subscribe(state => {
      this.user = state;
      console.log('===>>>  ', this.user);
    });
    this.store.dispatch(UserActions.getUser());
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}

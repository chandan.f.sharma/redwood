import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { userReducer } from './store/user.reducer';
import { UserEffect } from './store/user.effects';
import { UserComponent } from './components/user/user.component';
import { CookieService } from 'ngx-cookie-service';
import { UserRoutingModule } from './user-routing.module';



@NgModule({
  declarations: [UserComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    StoreModule.forFeature('userFeature', {user: userReducer}),
    EffectsModule.forFeature([UserEffect])
  ],
  exports: [
    UserComponent
  ],
  providers: [
    CookieService
  ]
})
export class UserModule { }

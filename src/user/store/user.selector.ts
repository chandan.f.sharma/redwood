import { createFeatureSelector, createSelector } from '@ngrx/store';
import { User } from '../models/user.model';


export const selectUserState = createFeatureSelector('userFeature');
export const selectUser = createSelector(selectUserState, (state: User) => state);

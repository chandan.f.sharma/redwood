import { createReducer, on } from '@ngrx/store';
import * as UserActions from './user.action';
import { User } from '../models/user.model';
import { plainToClass } from 'class-transformer';

export const initialState: User = plainToClass(User, {});

const reducer = createReducer(initialState,
  on(UserActions.setUser, (state, payload) => (plainToClass(User, {...state, ...payload})))
);

export function userReducer(state, action) {
  return reducer(state, action);
}

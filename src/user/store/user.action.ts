import { createAction, props, Action } from '@ngrx/store';
import { User } from '../models/user.model';

export const getUser = createAction('[UserComponent] GetUser');
export const setUser = createAction('[UserComponent] SetUser', props<User>());

import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import * as UserActions from './user.action';
import { User } from '../models/user.model';
import { plainToClass } from 'class-transformer';

@Injectable()
export class UserEffect {
    user: User = null;
    constructor(private action$: Actions, private cookieService: CookieService) {
        const objUser = this.cookieService.get('User');
        if (objUser) {
            this.user = JSON.parse(objUser);
        }
    }

    setUserEffect = createEffect(() =>
        this.action$.pipe(
            ofType(UserActions.getUser),
            map(() => (
                UserActions.setUser(this.user))
            )
        ));
}

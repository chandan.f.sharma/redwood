import {Expose, Transform } from 'class-transformer';

export class User {
    @Expose()
    id: number;

    @Expose()
    name: string;

    @Expose()
    avatar: string;

    @Expose()
    permission: number;
}

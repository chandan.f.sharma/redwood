// import { Injectable, ɵConsole } from '@angular/core';
// import { CanActivate } from '@angular/router';
// import { Observable, of, Subject } from 'rxjs';
// import { Store } from '@ngrx/store';
// import { WorkLoad } from '@shared-module/models/workload.model';
// import * as SharedSelector from '../../shared/store/shared.selector';
// import * as SharedActions from '../../shared/store/shared.action';
// import { filter, take, switchMap, catchError, takeUntil, tap} from 'rxjs/operators';


// @Injectable({
//   providedIn: 'root'
// })
// export class WorkloadsService implements CanActivate {

//   ngUnsubscribe: Subject<void> = new Subject<void>();

//   constructor(private store: Store<WorkLoad>) { }

//   getFromStoreOrAPI(): Observable<any> {
//     return this.store
//       .select(SharedSelector.selectWorkLoads)
//       .pipe(takeUntil(this.ngUnsubscribe))
//       .pipe(
//         tap((data: any) => {
//         if (!data.length) {
//           this.store.dispatch(SharedActions.getAllWorkloads());
//         }})
//       , filter((data: any) => data.length)
//       , take(1));
//   }

//   canActivate(): Observable<boolean> {
//     return this.getFromStoreOrAPI()
//     .pipe(
//       switchMap(() => of(true)),
//       catchError(() => of(false)));
//     }
// }

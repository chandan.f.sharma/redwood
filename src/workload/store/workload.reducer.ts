import { createReducer, on } from '@ngrx/store';
import * as WorkloadActions from './workload.action';
import { plainToClass } from 'class-transformer';
import { WorkloadState } from '@workload-module/models/state.model';

export const initialState: WorkloadState = plainToClass(WorkloadState, {});

const reducer = createReducer(
  initialState,
  on(WorkloadActions.addWorkloadSuccess, state => ({
    ...state,
    addWorkloadSuccess: true,
    addWorkloadFailure: null
  })),
  on(WorkloadActions.addUpdateParameterSuccess, state => ({
    ...state,
    addUpdateParameterSuccess: true,
    addUpdateParameterFailure: null
  })),
  on(WorkloadActions.addUpdateCParameterSuccess, state => ({
    ...state,
    addUpdateCParameterSuccess: true,
    addUpdateCParameterFailure: null
  })),
  on(WorkloadActions.addWorkloadFailure, (state, payload) => ({
    ...state,
    ...payload
  })),
  on(WorkloadActions.addUpdateParameterFailure, (state, payload) => ({
    ...state,
    ...payload
  })),
  on(WorkloadActions.addUpdateCParameterFailure, (state, payload) => ({
    ...state,
    ...payload
  })),
  on(WorkloadActions.updateStatus, (state, payload) => ({
    ...state,
    ...payload
  }))
);

export function workloadReducer(state, action) {
  return reducer(state, action);
}

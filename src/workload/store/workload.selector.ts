import { createFeatureSelector, createSelector } from '@ngrx/store';
import { WorkloadState } from '@workload-module/models/state.model';

export const selectWorkLoadFeature = createFeatureSelector('workloadFeature');

export const selectWorkloadState = createSelector(
  selectWorkLoadFeature,
  (state: { workload: WorkloadState }) => state.workload);
selectWorkloadState.prototype.class = WorkloadState;

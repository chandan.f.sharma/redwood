import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { exhaustMap, catchError, switchMap } from 'rxjs/operators';
import { WorkloadService } from '@workload-module/services/workload.service';
import * as WorkloadActions from './workload.action';
import * as SharedActions from '@shared-module/store/shared.action';
import { of } from 'rxjs';

@Injectable()
export class WorkloadEffect {
  constructor(
    private actions$: Actions,
    private workloadService: WorkloadService
  ) {}

  createNewWorkloadEffect = createEffect(() =>
    this.actions$.pipe(
      ofType(WorkloadActions.addNewWorkload),
      exhaustMap(payload => {
        return this.workloadService.addNewWorkload(payload.newWorkload).pipe(
          switchMap(resp => [
            SharedActions.addNewWorkload({ newWorkload: resp }),
            WorkloadActions.addWorkloadSuccess()
          ]),
          catchError((error: Error) =>
            of(
              WorkloadActions.addWorkloadFailure({
                addWorkloadFailure: error.message
              })
            )
          )
        );
      })
    )
  );

  createNewParameterEffect = createEffect(() =>
    this.actions$.pipe(
      ofType(WorkloadActions.addNewParameter),
      exhaustMap(payload => {
        return this.workloadService.addNewParameter(payload).pipe(
          switchMap(resp => [
            SharedActions.getParametersForWorkload({
              workloadId: payload.workloadId
            }),
            WorkloadActions.addUpdateParameterSuccess()
          ]),
          catchError((error: Error) => of(WorkloadActions.addUpdateParameterFailure({
            addUpdateParameterFailure: error.message
          })))
        );
      })
    )
  );

  updateParameterEffect = createEffect(() =>
    this.actions$.pipe(
      ofType(WorkloadActions.updateParameter),
      exhaustMap(payload => {
        return this.workloadService.updateParameter(payload).pipe(
          switchMap(resp => [
            SharedActions.getParametersForWorkload({
              workloadId: payload.workloadId
            }),
            WorkloadActions.addUpdateParameterSuccess()
          ]),
          catchError((error: Error) => of(WorkloadActions.addUpdateParameterFailure({
            addUpdateParameterFailure: error.message
          })))
        );
      })
    )
  );
}

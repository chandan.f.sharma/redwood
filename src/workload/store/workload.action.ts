import { createAction, props } from '@ngrx/store';
import { WorkLoad } from '@shared-module/models/workload.model';
import { Parameter } from '@shared-module/models/parameter.model';
import { CParameter } from '@shared-module/models/c-parameter.model';

export const addNewWorkload = createAction(
  '[WorkLoadModule] addNewWorkload',
  props<{ newWorkload: WorkLoad }>()
);

export const addWorkloadSuccess = createAction(
  '[WorkLoadModule] addWorkloadSuccess'
);

export const addWorkloadFailure = createAction(
  '[WorkLoadModule] addWorkloadFailure',
  props<{ addWorkloadFailure: string; }>()
);

export const addNewParameter = createAction(
  '[WorkLoadModule] addNewParameter',
  props<{ parameter: Parameter; workloadId: number }>()
);

export const updateParameter = createAction(
  '[WorkLoadModule] updateParameter',
  props<{ parameter: Parameter; workloadId: number }>()
);

export const addUpdateParameterSuccess = createAction(
  '[WorkLoadModule] addUpdateParameterSuccess'
);

export const addUpdateParameterFailure = createAction(
  '[WorkLoadModule] addUpdateParameterFailure',
  props<{ addUpdateParameterFailure: string; }>()
);

export const addNewCParameter = createAction(
  '[WorkLoadModule] addNewCParameter',
  props<{ parameter: CParameter; workloadId: number }>()
);

export const updateCParameter = createAction(
  '[WorkLoadModule] updateCParameter',
  props<{ parameter: CParameter; workloadId: number }>()
);

export const addUpdateCParameterSuccess = createAction(
  '[WorkLoadModule] addUpdateCParameterSuccess'
);

export const addUpdateCParameterFailure = createAction(
  '[WorkLoadModule] addUpdateCParameterFailure',
  props<{ addUpdateCParameterFailure: string; }>()
);

export const updateStatus = createAction(
  '[WorkLoadModule] updateStatus',
  props<{
    addWorkloadSuccess?: boolean;
    addUpdateParameterSuccess?: boolean;
    addUpdateCParameterSuccess?: boolean;
    addWorkloadFailure?: string;
    addUpdateParameterFailure?: string;
    addUpdateCParameterFailure?: string;
  }>()
);

import { Component, OnInit, OnDestroy } from '@angular/core';

import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { WorkLoad, WorkLoads } from '@shared-module/models/workload.model';
import * as SharedActions from '@shared-module/store/shared.action';
import * as SharedSelectors from '@shared-module/store/shared.selector';
import * as WorkloadActions from '../../store/workload.action';
import * as WorkloadSelectors from '../../store/workload.selector';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilitiesService } from 'src/app/services/utilities.service';
import { ModalService } from '@shared-module/services/modal.service';
import { WorkLoadConstants } from '@workload-module/const/const';
import { WorkloadState } from '@workload-module/models/state.model';

@Component({
  selector: 'app-workload-landing',
  templateUrl: './workload-landing.component.html',
  styleUrls: ['./workload-landing.component.scss']
})
export class WorkloadLandingComponent implements OnInit, OnDestroy {
  workloads: WorkLoad[];

  newWorkloadModalId: string;

  newWorkloadName: string;

  addWorkloadError: string;

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private store: Store<any>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private utilitiesService: UtilitiesService,
    private modalService: ModalService
  ) {}

  ngOnInit() {
    this.utilitiesService.listenToStateStore(
      this.store,
      [SharedSelectors.selectWorkLoads, WorkloadSelectors.selectWorkloadState],
      [],
      { state: {callBack: this.storeCallBack.bind(this)}, subject: this.ngUnsubscribe }
    );
    this.store.dispatch(SharedActions.getAllWorkloads());
    this.newWorkloadModalId = WorkLoadConstants.MODALS.NEW_WORKLOAD;
    this.newWorkloadName = '';
  }
  /**
   * call back function to return updated store data
   * @param type data type
   * @param value data value
   */
  storeCallBack(type: string, value: WorkLoad[] | WorkloadState) {
    switch (type) {
      case WorkLoads.name:
        this.workloads = value as WorkLoad[];
        break;
      case WorkloadState.name:
          const state = value as WorkloadState;
          if (state.addWorkloadSuccess) {
            const newName = this.newWorkloadName;
            this.newWorkloadName = '';
            const newId = this.workloads.find(w => w.name === newName).id;
            this.closeCreateWorkloadModal();
            this.showDetails(newId);

          } else if (state.addWorkloadFailure) {
            this.addWorkloadError = state.addWorkloadFailure;
          }
          break;
    }
  }
  /**
   * redirect to workload details on click
   * @param id workload id
   */
  showDetails(id: number) {
    this.router.navigate(['./WorkloadDetails', id], {
      relativeTo: this.activatedRoute
    });
  }
  /**
   * method to open work load modal
   */
  openCreateWorkloadModal() {
    this.modalService.openModal(this.newWorkloadModalId);
  }
  /**
   * method to close work load modal
   */
  closeCreateWorkloadModal() {
    this.modalService.closeModal(this.newWorkloadModalId);
  }
  /**
   * method to store newly created workload name
   */
  saveWorkloadName() {
    const newWorkLoad: WorkLoad = {
      id: null,
      name: this.newWorkloadName,
      parameters: []
    };
    this.store.dispatch(
      WorkloadActions.addNewWorkload({ newWorkload: newWorkLoad })
    );
  }
  /**
   * method to destroy subscription and empty store
   */
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
    this.store.dispatch(WorkloadActions.updateStatus({addWorkloadSuccess: false, addWorkloadFailure: ''}));
  }
}

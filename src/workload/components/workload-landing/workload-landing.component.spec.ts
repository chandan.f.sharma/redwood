import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WorkloadLandingComponent } from './workload-landing.component';
import { StoreModule, Store } from '@ngrx/store';
import { sharedReducer } from '@shared-module/store/shared.reducer';
import { UtilitiesService } from '@app-module/services/utilities.service';
import { ModalService } from '@shared-module/services/modal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import * as WorkloadActions from '../../store/workload.action';
import { FakeUtilService, FakeModalService, RouterStub, MockStore, FakeActivatedRoute } from 'src/mocks/angular-mock';
import { WorkLoad } from '@shared-module/models/workload.model';
import { WorkloadState } from '@workload-module/models/state.model';

describe('WorkloadLandingComponent', () => {
  let component: WorkloadLandingComponent;
  let fixture: ComponentFixture<WorkloadLandingComponent>;
  let router: Router;
  let service: ModalService;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkloadLandingComponent ],
      imports: [
        StoreModule.forRoot(sharedReducer)
      ],
      providers: [
        {provide: UtilitiesService, useClass: FakeUtilService},
        {provide: ModalService, useClass: FakeModalService},
        {provide: Router, useClass: RouterStub},
       {provide: ActivatedRoute,  useClass: FakeActivatedRoute}],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkloadLandingComponent);
    component = fixture.componentInstance;
    store = fixture.debugElement.injector.get(Store);
    router =  fixture.debugElement.injector.get(Router);
    service = fixture.debugElement.injector.get(ModalService);
    fixture.detectChanges();
  });

  it('should create', () => {
    component.storeCallBack('WorkLoads', [workload]);
    expect(component).toBeTruthy();
  });

  it('should update store value when callback', () => {
    component.newWorkloadName = 'abc';
    component.workloads = [workload];
    component.storeCallBack('WorkloadState', workloadState);
    expect(component).toBeTruthy();
  });

  it('should update store value', () => {
    component.storeCallBack('WorkloadState', workloadStates);
    expect(component).toBeTruthy();
  });

  it('should route to show Details', () => {
    const spy = jest.spyOn(component, 'showDetails');
    component.showDetails(1);
    const navigate = jest.spyOn(router, 'navigate');
    router.navigate([]);
    expect(spy).toHaveBeenCalledTimes(1);
    expect(navigate).toHaveBeenCalled();
  });

  it('should open workload modal', () => {
    const spy = jest.spyOn(component, 'openCreateWorkloadModal');
    jest.fn(service.openModal);
    component.openCreateWorkloadModal();
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should open workload modal', () => {
    const spy = jest.spyOn(component, 'closeCreateWorkloadModal');
    jest.fn(service.closeModal);
    component.closeCreateWorkloadModal();
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should open workload modal', () => {
    const spy = jest.spyOn(component, 'saveWorkloadName');
    jest.fn(component.closeCreateWorkloadModal);
    spyOn(store, 'dispatch');
    store.dispatch(WorkloadActions.addNewWorkload);
    component.saveWorkloadName();
    expect(spy).toHaveBeenCalledTimes(1);
    expect(store.dispatch).toHaveBeenCalledWith(WorkloadActions.addNewWorkload);
  });
});

const workload: WorkLoad = {
  id: 1,
  name: 'abc',
  parameters: [{
      id: 1,
      key: '',
      type: true,
      order: 1,
      groupName: undefined,
      enabled: true,
      hidden: true,
      required: true,
      heading: '',
      title: '',
      interaction: undefined,
      hint: '',
      default: '',
      placeholder: '',
      class: '',
      options: [],
      actions: undefined,
      value: undefined,
      validations: [],
      parameters: [],
      beginAtNewLine: true,
      isDropDown: true,
      dropdownOptions: [],
      isInputField: true,
      regExes: [],
      errors: []
  }]
};

const workloadState: WorkloadState = {
  addWorkloadSuccess: true,
  addWorkloadFailure: '',
  addUpdateCParameterFailure: '',
  addUpdateCParameterSuccess: true,
  addUpdateParameterFailure: '',
  addUpdateParameterSuccess: true
};
const workloadStates: WorkloadState = {
  addWorkloadSuccess: false,
  addWorkloadFailure: 'error',
  addUpdateCParameterFailure: '',
  addUpdateCParameterSuccess: true,
  addUpdateParameterFailure: '',
  addUpdateParameterSuccess: true
};

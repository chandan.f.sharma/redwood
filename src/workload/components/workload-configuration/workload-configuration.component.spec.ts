import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WorkloadConfigurationComponent } from './workload-configuration.component';
import { NO_ERRORS_SCHEMA} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store, StoreModule } from '@ngrx/store';
import { ModalService } from '@shared-module/services/modal.service';
import { UtilitiesService } from '@app-module/services/utilities.service';
import { sharedReducer } from '@shared-module/store/shared.reducer';
import { FakeUtilService, FakeModalService, RouterStub, MockStore, FakeActivatedRoute } from 'src/mocks/angular-mock';
import { plainToClass } from 'class-transformer';
import { Parameter } from '@shared-module/models/parameter.model';
import { WorkLoad } from '@shared-module/models/workload.model';

describe('WorkloadConfigurationComponent', () => {
  let component: WorkloadConfigurationComponent;
  let fixture: ComponentFixture<WorkloadConfigurationComponent>;
  let service: ModalService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkloadConfigurationComponent],
      imports: [
        StoreModule.forRoot(sharedReducer)
      ],
      providers: [UtilitiesService,
        {provide: UtilitiesService, useClass: FakeUtilService},
        {provide: ModalService, useClass: FakeModalService},
        {provide: Router, useClass: RouterStub},
        {
          provide: Store,
          useClass: MockStore
        },
       {provide: ActivatedRoute,  useClass: FakeActivatedRoute}],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkloadConfigurationComponent);
    component = fixture.componentInstance;
    service = fixture.debugElement.injector.get(ModalService);
    fixture.detectChanges();
  });

  it('should create', () => {
    component.storeCallBack('WorkLoad', workload);
    expect(component).toBeTruthy();
  });

  it('should init component', () => {
    component.storeCallBack('WorkLoad', undefined);
    expect(component).toBeTruthy();
  });

  it('should check open modal', () => {
    const spy = jest.spyOn(component, 'openModal');
    component.openModal('1');
    jest.spyOn(service, 'openModal');
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should check close modal', () => {
    const spy = jest.spyOn(component, 'closeModal');
    component.closeModal('1');
    spyOn(service, 'closeModal');
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should update order when down arrow clicked', () => {
    const spy = jest.spyOn(component, 'updateOrder');
    component.updateOrder(workload.parameters);
    expect(spy).toHaveBeenCalledTimes(1);
  });

});
const workload: WorkLoad = {
  id: 1,
  name: 'VM',
  parameters: [{
      id: 1,
      key: '',
      type: true,
      order: 1,
      groupName: undefined,
      enabled: true,
      hidden: true,
      required: true,
      heading: '',
      title: '',
      interaction: undefined,
      hint: '',
      default: '',
      placeholder: '',
      class: '',
      options: [],
      actions: undefined,
      value: undefined,
      validations: [],
      parameters: [],
      beginAtNewLine: true,
      isDropDown: true,
      dropdownOptions: [],
      isInputField: true,
      regExes: [],
      errors: []
  }]
};

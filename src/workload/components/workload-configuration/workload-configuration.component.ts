import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalService } from '@shared-module/services/modal.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { WorkLoad } from '@shared-module/models/workload.model';
import * as SharedSelector from '@shared-module/store/shared.selector';
import * as SharedActions from '@shared-module/store/shared.action';
import * as WorkloadSelector from '@workload-module/store/workload.selector';
import * as WorkloadActions from '@workload-module/store/workload.action';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { UtilitiesService } from '@app-module/services/utilities.service';
import { WorkLoadConstants } from '@workload-module/const/const';
import { Parameter } from '@shared-module/models/parameter.model';
import { SharedState } from '@shared-module/models/state.model';
import { WorkloadState } from '@workload-module/models/state.model';

@Component({
  selector: 'app-workload-configuration',
  templateUrl: './workload-configuration.component.html',
  styleUrls: ['./workload-configuration.component.scss']
})
export class WorkloadConfigurationComponent implements OnInit, OnDestroy {
  id: number;

  workload: WorkLoad;

  ngUnsubscribe: Subject<void> = new Subject<void>();

  CONSTANTS = WorkLoadConstants;

  parameterId: number;

  constructor(
    private modalService: ModalService,
    private utilitiesService: UtilitiesService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private store: Store<any>
  ) {}

  ngOnInit() {
    this.activatedRoute.params
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(params => {
        this.id = +params.id; // (+) converts string 'id' to a number
        //  select the Workload Details by its selector by passing the id
        this.utilitiesService.listenToStateStore(
          this.store,
          [SharedSelector.selectWorkLoadWithId, WorkloadSelector.selectWorkloadState],
          [{ id: this.id }],
          {
            state: { callBack: this.storeCallBack.bind(this) },
            subject: this.ngUnsubscribe
          }
        );
      });
  }

  /**
   * Call back method when store updates
   * @param type: string to determine what is changed
   * @param value: new value
   */
  storeCallBack(type: string, value: WorkLoad | WorkloadState) {
    switch (type) {
      case WorkloadState.name:
        const state = value as WorkloadState;
        if (state.addUpdateParameterSuccess) {
          console.log(state);
          this.closeModal(this.CONSTANTS.MODALS.NEW_PARAMETER);
          this.store.dispatch(
            WorkloadActions.updateStatus({
              addUpdateParameterSuccess: false,
              addUpdateParameterFailure: ''
            })
          );
        }
        break;
      case WorkLoad.name:
        this.workload = value as WorkLoad;
        if (this.workload) {
          this.store.dispatch(
            SharedActions.getParametersForWorkload({ workloadId: this.id })
          );
        } else {
          this.router.navigate(['../../'], {
            relativeTo: this.activatedRoute
          });
        }
        break;
    }
  }

  addOrEditParameter(parameterId: number) {
    this.parameterId = parameterId;
    this.openModal(this.CONSTANTS.MODALS.NEW_PARAMETER);
  }

  deleteParameter(id: number) {console.log(this.workload.parameters.map(p => p.id)); }

  /**
   * method to update param index
   * @param parameters: Array of parameters whose order property changed
   */
  updateOrder(parameters: Parameter[]) {}

  /**
   * Opens a modal
   * @param id : modal id
   */
  openModal(id: string) {
    this.modalService.openModal(id);
  }

  /**
   * Closes a modal
   * @param id : modal id
   */
  closeModal(id: string) {
    this.modalService.closeModal(id);
  }

  /**
   * method to destroy subscription and empty store
   */
  ngOnDestroy() {
    this.store.dispatch(
      SharedActions.updateWorkload({ workload: this.workload })
    );
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}

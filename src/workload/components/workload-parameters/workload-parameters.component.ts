import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Parameter } from '@shared-module/models/parameter.model';
import { UtilitiesService } from '@app-module/services/utilities.service';

@Component({
  selector: 'app-workload-parameters',
  templateUrl: './workload-parameters.component.html',
  styleUrls: ['./workload-parameters.component.scss']
})
export class WorkloadParametersComponent implements OnInit {
  @Input() parameters: Parameter[];

  @Output() edit: EventEmitter<number> = new EventEmitter<number>();

  @Output() delete: EventEmitter<number> = new EventEmitter<number>();

  @Output() order: EventEmitter<Parameter[]> = new EventEmitter<
    Parameter[]
  >();

  selectedDict: { [key: string]: boolean };

  selectedParameter: Parameter;

  constructor(private utilitiesService: UtilitiesService) {}

  ngOnInit() {
    this.selectedDict = {};
  }

  selectParameter(param: Parameter) {
    this.selectedDict = {};
    if (this.selectedParameter) {
      if (this.selectedParameter.key !== param.key) {
        this.selectedDict[param.key] = true;
        this.selectedParameter = param;
      } else {
        this.selectedParameter = null;
      }
    } else {
      this.selectedDict[param.key] = true;
      this.selectedParameter = param;
    }
  }

  editParameter(id: number) {
    this.edit.emit(id);
  }

  deleteParameter(id: number) {
    this.delete.emit(id);
  }

  changeOrder(up: boolean) {
    if (!this.selectedParameter) {
      return;
    }
    const param = this.selectedParameter;
    let parameters = this.selectParameters(param);
    const paramIndex = parameters.findIndex(p => p.key === param.key);

    const adjParam: Parameter = up
      ? paramIndex > 0
        ? parameters[paramIndex - 1]
        : null
      : paramIndex < parameters.length - 1
      ? parameters[paramIndex + 1]
      : null;
    if (adjParam) {
      adjParam.order += param.order;
      param.order = adjParam.order - param.order;
      adjParam.order = adjParam.order - param.order;
      parameters = this.utilitiesService.sortBy(parameters, 'order');
      this.order.emit([param, adjParam]);
    }
  }

  selectParameters(parameter: Parameter): Parameter[] {
    const parameters: Parameter[] = parameter.groupName
      ? this.parameters.find(p => p.key === parameter.groupName).parameters
      : this.parameters;
    return parameters;
  }
}

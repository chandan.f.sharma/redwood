import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkloadParametersComponent } from './workload-parameters.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('WorkloadParametersComponent', () => {
  let component: WorkloadParametersComponent;
  let fixture: ComponentFixture<WorkloadParametersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkloadParametersComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkloadParametersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

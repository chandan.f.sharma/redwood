import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParameterConfigurationComponent } from './parameter-configuration.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { plainToClass } from 'class-transformer';
import { ParameterConfiguration } from '@workload-module/models/parameter-configuration.model';
import { Parameter } from '@shared-module/models/parameter.model';
import { StoreModule, Store } from '@ngrx/store';
import { sharedReducer } from '@shared-module/store/shared.reducer';
import { MockStore } from 'src/mocks/angular-mock';

describe('ParameterConfigurationComponent', () => {
  let component: ParameterConfigurationComponent;
  let fixture: ComponentFixture<ParameterConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParameterConfigurationComponent ],
      imports: [
        StoreModule.forRoot(sharedReducer)
      ],
      providers: [
        {
          provide: Store,
          useClass: MockStore
        }],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParameterConfigurationComponent);
    component = fixture.componentInstance;
    component.paramModel = plainToClass(ParameterConfiguration, {});
    component.selectedValueForActions = '';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update value on action change when param options', () => {
    jest.spyOn(component, 'handleChange');
    component.handleChange('options');
    expect(component.selectedValueForActions).toEqual('');
  });

  it('should set parameters when param target', () => {
    component.paramModel.actions = {'': [{prop: '', target: '', value: true}]};
    const spy = jest.spyOn(component, 'handleChange');
    component.handleChange('target', '', '');
    jest.spyOn(component, 'updateAction');
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should set parameters when param action', () => {
    component.paramModel.actions = {'': [{prop: '', target: '', value: true}]};
    const spy = jest.spyOn(component, 'handleChange');
    component.handleChange('action', '', '');
    jest.spyOn(component, 'updateAction');
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should set parameters when param action', () => {
    jest.spyOn(component, 'addAction');
    component.addAction();
    expect(component.paramModel.actions).toEqual({'': [{prop: '', target: '', value: ''}]});
  });

});

import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import {
  Parameter,
  ParameterAction,
  Validation
} from "@shared-module/models/parameter.model";
import * as WorkloadActions from "../../store/workload.action";
import { plainToClass } from "class-transformer";
import { Option } from "../../../shared/models/option.model";
import {
  ParameterConfiguration,
  ConfigurationAction
} from "@workload-module/models/parameter-configuration.model";
import { Store } from "@ngrx/store";
import { WorkLoads } from "@shared-module/models/workload.model";

@Component({
  selector: "app-parameter-configuration",
  templateUrl: "./parameter-configuration.component.html",
  styleUrls: ["./parameter-configuration.component.scss"]
})
export class ParameterConfigurationComponent implements OnInit {
  paramModel: ParameterConfiguration;

  actionsForSourceValue: ConfigurationAction[];

  selectedValueForActions: string;

  enableSubmit: boolean;

  error: { [key: string]: string };

  private paramId: number;

  @Input() modalId: string;

  @Input() workload: WorkLoads;

  @Input() set parameterId(id: number) {
    this.paramId = id;
    this.initModalValues();
  }

  get parameterId(): number {
    return this.paramId;
  }

  @Output() modalClose: EventEmitter<void> = new EventEmitter<void>();

  constructor(private store: Store<any>) {}

  ngOnInit() {}

  initModalValues() {
    this.selectedValueForActions = "";
    this.enableSubmit = false;
    this.error = {};
    const param = this.parameterId
      ? this.workload.parameters.find(p => p.id === this.parameterId)
      : {};
    this.paramModel = plainToClass(
      ParameterConfiguration,
      { ...param },
      { excludeExtraneousValues: true }
    );
    this.paramModel.keys = [];
    this.paramModel.groups = [];
    this.workload.parameters.forEach((p, i) => {
      const option = plainToClass(Option, { key: i, value: p.key });
      if (p.type) {
        this.paramModel.groups.push(option);
      } else if (option.value !== this.paramModel.key) {
        this.paramModel.keys.push(option);
      }
    });
  }

  handleChange(param: string, ...args) {
    switch (param) {
      case "options":
        this.paramModel.options = this.paramModel.definedOptions
          .map(o => o.value)
          .join(",");
        this.selectedValueForActions = "";
        this.showActionsForValue();
        this.updateActionsOnOptionsChange();
        break;
      case "target":
        args.reduce((index, target) => {
          this.updateAction(index, { target });
        });
        break;
      case "action":
        args.reduce((index, action) => {
          this.updateAction(index, this.paramModel.getPropAndValue(action));
        });
        break;
      case "key":
        const exist = this.paramModel.keys.find(
          k => k.value === this.paramModel.key
        );
        delete this.error.key;
        if (exist) {
          this.error.key = "Choose different name.";
        }

        break;
    }
    this.enableSubmittion();
  }

  enableSubmittion() {
    this.enableSubmit = false;
    const conditionMandatory =
      this.paramModel.key && Object.keys(this.error).length === 0;
    const conditionGroup = this.paramModel.type;
    const conditionDropdown =
      this.paramModel.interaction === 0 &&
      this.paramModel.definedOptions.length;
    const conditionInput = this.paramModel.interaction === 1;

    if (conditionMandatory) {
      if (conditionGroup || conditionInput || conditionDropdown) {
        this.enableSubmit = true;
      }
    }
  }

  showActionsForValue() {
    this.actionsForSourceValue = this.paramModel.definedActions(
      this.selectedValueForActions
    );
  }

  addAction() {
    let arrActions = this.paramModel.actions[this.selectedValueForActions]
      ? this.paramModel.actions[this.selectedValueForActions]
      : [];
    arrActions = [
      ...arrActions,
      plainToClass(ParameterAction, { target: "", prop: "", value: "" })
    ];
    this.paramModel.actions[this.selectedValueForActions] = arrActions;
    this.showActionsForValue();
  }

  removeAction(index: number) {
    let arrActions = this.paramModel.actions[this.selectedValueForActions];
    arrActions = arrActions.filter((a, i) => i !== index);
    this.paramModel.actions[this.selectedValueForActions] = arrActions;
    this.showActionsForValue();
  }

  updateAction(index: number, action?: ParameterAction) {
    const arrActions = this.paramModel.actions[this.selectedValueForActions];
    let act = arrActions[index];
    act = { ...act, ...action };
    arrActions[index] = act;
  }

  updateActionsOnOptionsChange() {
    const keys = Object.keys(this.paramModel.actions);
    keys.forEach(k => {
      if (this.paramModel.options.indexOf(k) === -1) {
        delete this.paramModel.actions[k];
      }
    });
  }

  addValidation() {
    this.paramModel.validations = [
      ...this.paramModel.validations,
      { regex: "", error: "" }
    ];
  }

  removeValidation(index: number) {
    this.paramModel.validations = this.paramModel.validations.filter(
      (v, i) => i !== index
    );
  }

  updateValidation(i, validation?: Validation) {
    let v = this.paramModel.validations[i];
    v = { ...v, ...validation };
    this.paramModel.validations[i] = v;
  }

  onSubmit() {
    const parameter = plainToClass(Parameter, this.paramModel, {
      excludeExtraneousValues: true
    });
    if (!parameter.id) {
      this.setParameterOrder(parameter);
    }
    const dispatchAction = !this.paramModel.id
      ? WorkloadActions.addNewParameter({
          parameter,
          workloadId: this.workload.id
        })
      : WorkloadActions.updateParameter({
          parameter,
          workloadId: this.workload.id
        });
    this.store.dispatch(dispatchAction);
  }

  setParameterOrder(p: Parameter, group?: Parameter) {
    const parameters = group ? group.parameters : this.workload.parameters;
    const order = parameters.length
      ? parameters[parameters.length - 1].order + 1
      : 0;
    p.order = order;
  }

  reset() {
    this.modalClose.emit();
  }
}

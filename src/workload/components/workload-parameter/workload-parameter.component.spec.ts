import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkloadParameterComponent } from './workload-parameter.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('WorkloadParameterComponent', () => {
  let component: WorkloadParameterComponent;
  let fixture: ComponentFixture<WorkloadParameterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkloadParameterComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkloadParameterComponent);
    component = fixture.componentInstance;
    component.parameter = param[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
const param =  [{
  id: 1,
  key: '',
  type: true,
  order: 1,
  groupName: undefined,
  enabled: true,
  hidden: true,
  required: true,
  heading: '',
  title: '',
  interaction: undefined,
  hint: '',
  default: '',
  placeholder: '',
  class: '',
  options: [],
  actions: undefined,
  value: undefined,
  validations: [],
  parameters: [],
  beginAtNewLine: true,
  isDropDown: true,
  dropdownOptions: [],
  isInputField: true,
  regExes: [],
  errors: []
}];

import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Parameter } from '@shared-module/models/parameter.model';

@Component({
  selector: 'app-workload-parameter',
  templateUrl: './workload-parameter.component.html',
  styleUrls: ['./workload-parameter.component.scss']
})
export class WorkloadParameterComponent {
  @Input() parameter: Parameter;

  @Input() selected: boolean;

  @Output() edit: EventEmitter<void> = new EventEmitter();

  @Output() delete: EventEmitter<void> = new EventEmitter();

  constructor() {}

  handleEdit(e: Event) {
    e.stopImmediatePropagation();
    this.edit.emit();
  }

  handleDelete(e: Event) {
    e.stopImmediatePropagation();
    this.delete.emit();
  }
}

import { Expose, Transform, Type, plainToClass } from 'class-transformer';
import { Option } from '../../shared/models/option.model';
import {
  Action,
  ParameterAction,
  Validation
} from '@shared-module/models/parameter.model';
import { WorkLoad } from '@shared-module/models/workload.model';

export class ConfigurationAction {
  target: string;
  action: string;
}

export class ParameterConfiguration {
  @Expose()
  @Transform(value => value || [], {
    toClassOnly: true
  })
  keys: Option[];

  @Expose()
  @Transform(value => value || [], {
    toClassOnly: true
  })
  groups: Option[];

  @Expose()
  id: number;

  @Expose()
  @Transform(value => value || '', {
    toClassOnly: true
  })
  key: string;

  @Expose()
  @Transform(value => (typeof value !== 'boolean' ? false : value), {
    toClassOnly: true
  })
  type: boolean;

  @Expose()
  @Transform(value => value || '', {
    toClassOnly: true
  })
  groupName: string;

  @Expose()
  @Transform(value => (typeof value !== 'boolean' ? true : value), {
    toClassOnly: true
  })
  enabled: boolean;

  @Expose()
  @Transform(value => (typeof value !== 'boolean' ? false : value), {
    toClassOnly: true
  })
  hidden: boolean;

  @Expose()
  @Transform(value => (typeof value !== 'boolean' ? true : value), {
    toClassOnly: true
  })
  required: boolean;

  @Expose()
  @Transform(value => value || '', { toClassOnly: true })
  heading: string;

  @Expose()
  @Transform(value => value || '', { toClassOnly: true })
  title: string;

  @Expose()
  @Transform(value => value || '', { toClassOnly: true })
  hint: string;

  @Expose()
  @Transform(value => value || 1, { toClassOnly: true })
  interaction: number;

  @Expose()
  @Transform(value => value || '', { toClassOnly: true })
  default: string;

  @Expose()
  @Transform(value => value || '', { toClassOnly: true })
  placeholder: string;

  @Expose()
  @Transform(value => value || '', { toClassOnly: true })
  class: string;

  @Expose()
  @Transform(
    value => {
      return value && Array.isArray(value) ? value.join(',') : '';
    },
    { toClassOnly: true }
  )
  options: string;

  @Expose()
  @Type(() => Action)
  @Transform(value => value || {}, { toClassOnly: true })
  actions: Action;

  @Expose()
  @Type(() => Validation)
  @Transform(value => value || [], { toClassOnly: true })
  validations: Validation[];

  @Expose()
  get predefinedClasses(): Option[] {
    return [
      plainToClass(Option, { key: 0, value: 'full' }),
      plainToClass(Option, { key: 1, value: 'half' }),
      plainToClass(Option, { key: 3, value: 'third' }),
      plainToClass(Option, { key: 4, value: 'fourth' }),
      plainToClass(Option, { key: 5, value: 'new-line' }),
      plainToClass(Option, { key: 5, value: 'new-line half' }),
      plainToClass(Option, { key: 5, value: 'new-line third' }),
      plainToClass(Option, { key: 5, value: 'new-line fourth' })
    ];
  }

  @Expose()
  get definedOptions(): Option[] {
    const options = [];
    let count = 0;
    this.options.split(',').forEach(o => {
      if (o.trim().length && !options.find(opt => opt.value === o.trim())) {
        options.push(plainToClass(Option, { key: count++, value: o.trim() }));
      }
    });
    return options;
  }

  @Expose()
  definedActions(selectedOption: string): ConfigurationAction[] {
    return this.actions && this.actions[selectedOption]
      ? this.actions[selectedOption].map(a => {
          return {
            target: a.target,
            action: this.getActionName(a.prop, a.value)
          };
        })
      : [];
  }

  @Expose()
  get predefinedActions(): Option[] {
    return [
      plainToClass(Option, { key: 0, value: 'HIDE' }),
      plainToClass(Option, { key: 1, value: 'SHOW' })
    ];
  }

  @Expose()
  getActionName(prop: string, value: any): string {
    let name = '';
    if (prop === 'hidden' && value) {
      name = 'HIDE';
    } else if (prop === 'hidden' && !value) {
      name = 'SHOW';
    }
    return name;
  }

  @Expose()
  getPropAndValue(action: string): { prop: string; value: any } {
    switch (action) {
      case 'HIDE':
        return { prop: 'hidden', value: true };
      case 'SHOW':
        return { prop: 'hidden', value: false };
    }
  }
}

import { Expose } from 'class-transformer';

export class WorkloadState {
    @Expose()
    addWorkloadSuccess: boolean;

    @Expose()
    addUpdateParameterSuccess: boolean;

    @Expose()
    addUpdateCParameterSuccess: boolean;

    @Expose()
    addWorkloadFailure: string;

    @Expose()
    addUpdateParameterFailure: string;

    @Expose()
    addUpdateCParameterFailure: string;
}

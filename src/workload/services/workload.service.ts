import { Injectable } from '@angular/core';
import { WorkLoad } from '@shared-module/models/workload.model';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Parameter } from '@shared-module/models/parameter.model';

@Injectable({
  providedIn: 'root'
})
export class WorkloadService {
  constructor(private http: HttpClient) {}

  addNewWorkload(props: WorkLoad): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post('workloads', props, {
      headers
    });
  }

  addNewParameter(props: {
    parameter: Parameter;
    workloadId: number;
  }): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(
      'workloadparameters',
      { ...props.parameter, workloadId: props.workloadId },
      {
        headers
      }
    );
  }

  updateParameter(props: {
    parameter: Parameter;
    workloadId: number;
  }): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.put(
      'workloadparameters/' + props.parameter.id,
      { ...props.parameter, workloadId: props.workloadId },
      {
        headers
      }
    );
  }
}

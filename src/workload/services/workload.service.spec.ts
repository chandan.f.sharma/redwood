import { TestBed } from '@angular/core/testing';

import { WorkloadService } from './workload.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('WorkloadService', () => {

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule]
  }));

  it('should be created', () => {
    const service: WorkloadService = TestBed.get(WorkloadService);
    expect(service).toBeTruthy();
  });
});

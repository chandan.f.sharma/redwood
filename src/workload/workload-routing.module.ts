import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkloadLandingComponent } from './components/workload-landing/workload-landing.component';
import { WorkloadConfigurationComponent } from './components/workload-configuration/workload-configuration.component';
// import { WorkloadsService } from './service/workloads.service';

const routes: Routes = [
  { path: '', component: WorkloadLandingComponent },
  { path: 'WorkloadDetails', redirectTo: ''},
  { path: 'WorkloadDetails/:id', component: WorkloadConfigurationComponent
  /*,
   canActivate : [ WorkloadsService ]
  */
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkloadRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkloadLandingComponent } from './components/workload-landing/workload-landing.component';
import { WorkloadRoutingModule } from './workload-routing.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { WorkloadEffect } from './store/workload.effects';
import { workloadReducer } from './store/workload.reducer';
import { SharedModule } from '../shared/shared.module';
import { WorkloadConfigurationComponent } from './components/workload-configuration/workload-configuration.component';
import { AccordionComponent } from '@shared-module/components/accordion/accordion.component';
import { ParameterConfigurationComponent } from './components/parameter-configuration/parameter-configuration.component';
import { WorkloadParametersComponent } from './components/workload-parameters/workload-parameters.component';
import { WorkloadParameterComponent } from './components/workload-parameter/workload-parameter.component';

@NgModule({
  declarations: [
    WorkloadLandingComponent,
    WorkloadConfigurationComponent,
    AccordionComponent,
    ParameterConfigurationComponent,
    WorkloadParametersComponent,
    WorkloadParameterComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    WorkloadRoutingModule,
    StoreModule.forFeature('workloadFeature', {workload: workloadReducer}),
    EffectsModule.forFeature([WorkloadEffect])
  ],
  exports: []
})
export class WorkLoadModule {}

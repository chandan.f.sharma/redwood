export const WorkLoadConstants = {
    MODALS : {
        NEW_WORKLOAD: 'NEW_WORKLOAD',
        NEW_PARAMETER: 'NEW_PARAMETER',
        CALCULATED_PARAMETER: 'CALCULATED_PARAMETER'
    }

};

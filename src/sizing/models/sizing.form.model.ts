import {Expose, Transform, Type } from 'class-transformer';
import { IsNotEmpty, IsNumberString } from 'class-validator';

export class SizingForm {

    @Expose()
    @Transform(value => value || '', { toClassOnly: true })
    @IsNotEmpty({
        message: 'Name cannot be left blank',
        groups: ['step_1']
    })
    companyName: string;

    @Expose()
    @Transform(value => value || '', {toClassOnly: true})
    @IsNotEmpty({
        message: 'SizingState request name cannot be left blank',
        groups: ['step_1']
    })
    sizingRequestName: string;

    @Expose()
    @Transform(value => value || '', {toClassOnly: true})
    @IsNotEmpty({
        message: 'Industry name cannot be left blank',
        groups: ['step_1']
    })
    industry: string;

    @Expose()
    @Transform(value => value || '', {toClassOnly: true})
    @IsNotEmpty({
        message: 'location cannot be left blank',
        groups: ['step_1']
    })
    location: string;

    @Expose()
    @Transform(value => value || '', {toClassOnly: true})
    @IsNotEmpty({
        message: 'oppurtunity cannot be left blank',
        groups: ['step_1']
    })
    oppurtunity: string;

    @Expose()
    @Transform(value => value || '', {toClassOnly: true})
    @IsNumberString({
        message: 'DataRate can only be a number and cannot be left blank.',
        groups: ['step_2']
    })
    dataGrowthRate: number;

    @Expose()
    @Transform(value => value || '', {toClassOnly: true})
    @IsNotEmpty({
        message: 'storage Type cannot be left blank',
        groups: ['step_2']
    })
    storageType: string;

    @Expose()
    @Transform(value => value || '', {toClassOnly: true})
    @IsNotEmpty({
        message: 'workload cannot be left blank',
        groups: ['step_3']
    })
    workloadType: string;

    @Expose()
    @Transform(value => value || '', {toClassOnly: true})
    @IsNotEmpty({
        message: 'Workload name cannot be left blank',
        groups: ['step_3']
    })
    workloadName: string;

    @Expose()
    step: number;

}

export class Company {
    @Expose()
    id: number;

    @Expose()
    @Transform(value => value || '', { toClassOnly: true })
    name: string;
}

export class Industry extends Company {}
export class Location extends Company {}

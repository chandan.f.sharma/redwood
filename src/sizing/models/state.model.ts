import {Expose, Transform, Type, Exclude } from 'class-transformer';
import { SizingForm, Company, Industry, Location } from './sizing.form.model';

export class SizingStatus {
    @Expose()
    addRequestSuccess: number;

    @Expose()
    addRequestFailure: string;

    @Expose()
    updateRequestSuccess: boolean;

    @Expose()
    updateRequestFailure: string;
}

export class SizingState extends SizingStatus {

    @Expose()
    @Transform(value => value || [], { toClassOnly: true })
    @Type(() => Company)
    companies: Company[];

    @Expose()
    @Transform(value => value || [], { toClassOnly: true })
    @Type(() => Industry)
    industries: Industry[];

    @Expose()
    @Transform(value => value || [], { toClassOnly: true })
    @Type(() => Location)
    locations: Location[];

    @Expose()
    @Transform(value => value || {step: 1}, { toClassOnly: true })
    @Type(() => SizingForm)
    sizingForm: SizingForm;
}



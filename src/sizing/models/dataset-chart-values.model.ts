class CategoryData {
  name: string;
  data: number[];
}

export class LocalChartOptionValues {
  categories: string[];
  series: CategoryData[];
}

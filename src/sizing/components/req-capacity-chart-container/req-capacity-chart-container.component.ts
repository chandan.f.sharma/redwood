import { Component, OnInit } from '@angular/core';
import { LocalChartOptionValues } from '@sizing-module/models/dataset-chart-values.model';
import { CONSTANTS } from '@sizing-module/const/chart-constants';

@Component({
  selector: 'app-req-capacity-chart-container',
  templateUrl: './req-capacity-chart-container.component.html',
  styleUrls: ['./req-capacity-chart-container.component.scss']
})
export class ReqCapacityChartContainerComponent implements OnInit {
  ChartData: LocalChartOptionValues;

  constructor() {
    this.ChartData = {
      categories: ['Year0', 'Year1', 'Year2', 'Year3', 'Year4', 'Year5'],
      series: [
        {
          name: CONSTANTS.LegendName.UsableCapacity,
          data: [5, 3, 4, 7, 2, 1]
        },
        {
          name: CONSTANTS.LegendName.ReserveSpace,
          data: [2, 2, 3, 2, 1, 3]
        },
        {
          name: CONSTANTS.LegendName.TotalCapacity,
          data: [3, 4, 4, 2, 5, 3]
        }
      ]
    };
  }

  ngOnInit() {}
}

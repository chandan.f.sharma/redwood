import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReqCapacityChartContainerComponent } from './req-capacity-chart-container.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ReqCapacityChartContainerComponent', () => {
  let component: ReqCapacityChartContainerComponent;
  let fixture: ComponentFixture<ReqCapacityChartContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReqCapacityChartContainerComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReqCapacityChartContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

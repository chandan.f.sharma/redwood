import { Component, OnInit } from '@angular/core';
import { LocalChartOptionValues } from '@sizing-module/models/dataset-chart-values.model';
import { CONSTANTS } from '@sizing-module/const/chart-constants';

@Component({
  selector: 'app-dataset-chart-container',
  templateUrl: './dataset-chart-container.component.html',
  styleUrls: ['./dataset-chart-container.component.scss']
})
export class DatasetChartContainerComponent implements OnInit {

  ChartData: LocalChartOptionValues;

  constructor() {
    this.ChartData = {
      categories: [ CONSTANTS.xAxisLabels.UsableCapacity,
      CONSTANTS.xAxisLabels.ReserveCapacity,
      CONSTANTS.xAxisLabels.TotalUsableCapacity,
      CONSTANTS.xAxisLabels.ReplicationSeedingTime,
      CONSTANTS.xAxisLabels.ReplicationofIncrementals ],
      series : [{
        name: CONSTANTS.LegendName.NAS,
        data: [41.9, 42.9, 4.9, 40.9, 49.9]
    },
    {
      name: CONSTANTS.LegendName.VM,
      data: [61.9, 62.9, 6.9, 60.9, 69.9]
  }]
    };
  }

  ngOnInit() {
  }

}

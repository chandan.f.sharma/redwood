import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatasetChartContainerComponent } from './dataset-chart-container.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('DatasetChartContainerComponent', () => {
  let component: DatasetChartContainerComponent;
  let fixture: ComponentFixture<DatasetChartContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatasetChartContainerComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatasetChartContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

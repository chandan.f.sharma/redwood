import { Component, OnInit, Input } from '@angular/core';
import { Parameter } from '@shared-module/models/parameter.model';


@Component({
  selector: 'app-sizing-parameters',
  templateUrl: './sizing-parameters.component.html',
  styleUrls: ['./sizing-parameters.component.scss']
})
export class SizingParametersComponent implements OnInit {

  @Input() parameters: Parameter[];

  constructor() { }

  ngOnInit() {
  }

  /**
   * method to store user inputs
   * @param param parametr type value
   */
  onAnswer(param: Parameter) {
    if (param && param.actions && param.actions[param.value]) {
      const actions = param.actions[param.value];
      actions.forEach(a => {
        const target = this.parameters.find(p => p.key === a.target);
        target[a.prop] = a.value;
      });

    }

  }

}

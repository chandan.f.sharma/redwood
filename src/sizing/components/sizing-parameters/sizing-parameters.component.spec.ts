import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SizingParametersComponent } from './sizing-parameters.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('SizingParametersComponent', () => {
  let component: SizingParametersComponent;
  let fixture: ComponentFixture<SizingParametersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SizingParametersComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SizingParametersComponent);
    component = fixture.componentInstance;
    component.parameters = param;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should store answer input', () => {
    const spy = jest.spyOn(component, 'onAnswer');
    component.onAnswer(param[0]);
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should not store answer input when param empty', () => {
    const spy = jest.spyOn(component, 'onAnswer');
    component.onAnswer(undefined);
    expect(spy).toHaveBeenCalledTimes(1);
  });
});

const param =  [{
  id: 1,
  key: '',
  type: true,
  order: 1,
  groupName: undefined,
  enabled: true,
  hidden: true,
  required: true,
  heading: '',
  title: '',
  interaction: undefined,
  hint: '',
  default: '',
  placeholder: '',
  class: '',
  options: [],
  actions: {'': [{prop: '', target: '', value: true}]},
  value: '',
  validations: [],
  parameters: [],
  beginAtNewLine: true,
  isDropDown: true,
  dropdownOptions: [],
  isInputField: true,
  regExes: [],
  errors: []
}];

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import * as Highcharts from 'highcharts';
import { RequiredCapacityChartComponent } from './required-capacity-chart.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { GlobalChartOptions } from '@sizing-module/const/chart-constants';

xdescribe('RequiredCapacityChartComponent', () => {
  let component: RequiredCapacityChartComponent;
  let fixture: ComponentFixture<RequiredCapacityChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequiredCapacityChartComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequiredCapacityChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

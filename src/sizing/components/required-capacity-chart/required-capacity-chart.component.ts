import { Component, OnInit, Input } from '@angular/core';
import { LocalChartOptionValues } from '@sizing-module/models/dataset-chart-values.model';
import { GlobalChartOptions } from '@sizing-module/const/chart-constants';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-required-capacity-chart',
  templateUrl: './required-capacity-chart.component.html',
  styleUrls: ['./required-capacity-chart.component.scss']
})
export class RequiredCapacityChartComponent implements OnInit {

  globalChartOptions: Highcharts.Options;

  requiredCapacityChart: Highcharts.Chart;

  /**
   * Intercepting received chart data and updating inside Global chart data
   */
  @Input() set chartData(localChartOptions: LocalChartOptionValues) {

    // Setting X Axis Categories for labels
    const xAxis: Highcharts.XAxisOptions = this.globalChartOptions.xAxis as Highcharts.XAxisOptions;
    xAxis.categories = localChartOptions.categories;

    // Setting Y Axis stack labels
    const yAxis: Highcharts.YAxisOptions = this.globalChartOptions.yAxis as Highcharts.YAxisOptions;
    yAxis.stackLabels = { enabled: true };

    // Stacked Chart Configurations with Datalabels on each stack
    this.globalChartOptions.plotOptions.column.stacking = 'normal';
    this.globalChartOptions.plotOptions.column.dataLabels = { enabled: true };

    // Inserting Numerical data in series along with options
    this.globalChartOptions.series = localChartOptions.series as Highcharts.SeriesOptionsType[];
  }

  /**
   * Intialising our local chart data with global chart options
   */
  constructor() {
    this.globalChartOptions = JSON.parse(JSON.stringify(GlobalChartOptions)) as Highcharts.Options;
  }

  /**
   * Instantiating Highcharts with Chart options
   */
  ngOnInit() {
    this.requiredCapacityChart = Highcharts.chart('container-req-capacity', this.globalChartOptions);
  }
}

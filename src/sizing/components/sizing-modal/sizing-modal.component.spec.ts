import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SizingModalComponent } from './sizing-modal.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { UtilitiesService } from '@app-module/services/utilities.service';
import { FakeUtilService, FakeModalService, RouterStub } from 'src/mocks/angular-mock';
import { ModalService } from '@shared-module/services/modal.service';
import { Router } from '@angular/router';
import { sharedReducer } from '@shared-module/store/shared.reducer';
import { plainToClass } from 'class-transformer';
import { SizingForm } from '@sizing-module/models/sizing.form.model';
import { WorkLoad } from '@shared-module/models/workload.model';

describe('SizingModalComponent', () => {
  let component: SizingModalComponent;
  let fixture: ComponentFixture<SizingModalComponent>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SizingModalComponent ],
      imports: [
        StoreModule.forRoot(sharedReducer)
      ],
      providers: [
        {provide: UtilitiesService, useClass: FakeUtilService},
        {provide: ModalService, useClass: FakeModalService},
        {provide: Router, useClass: RouterStub}],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SizingModalComponent);
    component = fixture.componentInstance;
    router =  fixture.debugElement.injector.get(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    component.storeCallBack('SizingForm', '');
    expect(component).toBeTruthy();
  });

  it('should validate form', () => {
    const spy = jest.spyOn(component, 'performValidate');
    component.sizingFormData = form;
    component.performValidate(false);
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should throw error when form invalid', () => {
    component.sizingFormData = plainToClass(SizingForm, {});
    component.sizingFormData.step = 1;
    const spy = jest.spyOn(component, 'performValidate');
    component.performValidate('companyName');
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should throw error when form invalid', () => {
    component.sizingFormData = plainToClass(SizingForm, {});
    component.sizingFormData.step = 1;
    const spy = jest.spyOn(component, 'performValidate');
    component.performValidate('');
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should create sizing request', () => {
    const spy = jest.spyOn(component, 'createNewSizingRequest');
    component.createNewSizingRequest();
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should handle navigation', () => {
    const spy = jest.spyOn(component, 'handleNavigation');
    const navigate = jest.spyOn(router, 'navigate');
    router.navigate([]);
    component.handleNavigation('cancel');
    expect(spy).toHaveBeenCalledTimes(1);
    expect(navigate).toHaveBeenCalled();
  });

  it('should handle navigation with action back', () => {
    component.sizingFormData = form;
    const spy = jest.spyOn(component, 'handleNavigation');
    component.handleNavigation('back');
    expect(spy).toHaveBeenCalledTimes(1);
    expect(component.sizingFormData.step).toEqual(0);
  });

  it('should handle navigation with action next', () => {
    component.sizingFormData = form;
    const spy = jest.spyOn(component, 'handleNavigation');
    component.handleNavigation('next');
    expect(spy).toHaveBeenCalledTimes(1);
    expect(component.sizingFormData.step).toEqual(1);
  });

  it('should handle navigation with action submit', () => {
    component.sizingFormData = form;
    component.state = {WorkLoads: [workload]};
    const spy = jest.spyOn(component, 'handleNavigation');
    component.handleNavigation('submit');
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should handle navigation with action submit', () => {
    const spy = jest.spyOn(component, 'handleNavigation');
    component.handleNavigation('');
    expect(spy).toHaveBeenCalledTimes(1);
  });
});

const form = {
  companyName: 'Azure',
      sizingRequestName: 'New Test Request',
      industry: 'Data Solutions',
      location: 'England',
      oppurtunity: 'adds',
      dataGrowthRate: 3,
      storageType: 'Yes',
      workloadType: 'VM',
      workloadName: 'abc',
      step: 1
};
const workload: WorkLoad = {
  id: 1,
  name: 'VM',
  parameters: [{
      id: 1,
      key: '',
      type: true,
      order: 1,
      groupName: undefined,
      enabled: true,
      hidden: true,
      required: true,
      heading: '',
      title: '',
      interaction: undefined,
      hint: '',
      default: '',
      placeholder: '',
      class: '',
      options: [],
      actions: undefined,
      value: undefined,
      validations: [],
      parameters: [],
      beginAtNewLine: true,
      isDropDown: true,
      dropdownOptions: [],
      isInputField: true,
      regExes: [],
      errors: []
  }]
};

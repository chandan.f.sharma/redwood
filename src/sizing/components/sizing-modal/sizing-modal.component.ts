import { Component, OnInit } from '@angular/core';
import { SizingForm } from '../../models/sizing.form.model';
import { validate } from 'class-validator';
import * as SizingSelectors from '../../store/sizing.selector';
import * as SizingActions from '../../store/sizing.action';
import * as SharedSelectors from '../../../shared/store/shared.selector';
import * as SharedActions from '../../../shared/store/shared.action';
import { SizingState, SizingStatus } from '../../models/state.model';
import { Store } from '@ngrx/store';
import { SharedState, SharedStateStatus } from '@shared-module/models/state.model';
import { UtilitiesService } from 'src/app/services/utilities.service';
import { Subject } from 'rxjs';
import { WorkLoads } from '@shared-module/models/workload.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sizing-modal',
  templateUrl: './sizing-modal.component.html',
  styleUrls: ['./sizing-modal.component.scss']
})
export class SizingModalComponent implements OnInit {
  state: object;

  disableButton: boolean;

  errors: {};

  sizingFormData: SizingForm;

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private router: Router,
    private store: Store<any>,
    private utilitiesService: UtilitiesService
  ) {}

  ngOnInit() {
    this.errors = {};
    this.state = {
      getNames: arr => arr.map(item => item.name),
      callBack: this.storeCallBack.bind(this)
    };
    this.disableButton = true;
    this.utilitiesService.listenToStateStore(
      this.store,
      [
        SizingSelectors.selectCompanies,
        SizingSelectors.selectIndustries,
        SizingSelectors.selectLocations,
        SizingSelectors.selectSizingForm,
        SizingSelectors.selectSizingStatus,
        SharedSelectors.selectWorkLoads,
        SharedSelectors.selectSharedStateStatus
      ],
      [],
      { state: this.state, subject: this.ngUnsubscribe }
    );
    this.store.dispatch(SizingActions.getFormOptions());
    this.store.dispatch(SharedActions.getAllWorkloads());
  }

  /**
   * call back function to return updated store data
   * @param type data type
   * @param value data value
   */
  storeCallBack(type: string, value: any ) {
    switch (type) {
      case SizingForm.name:
        this.sizingFormData = value;
        break;
      case SizingStatus.name:
        const status = value as SizingStatus;
        if (status.addRequestSuccess) {
          this.store.dispatch(SizingActions.updateStatus({addRequestSuccess: null}));
          this.router.navigate([
            {
              outlets: {
                primary: ['Dashboard', 'SizingDetail', status.addRequestSuccess],
                forModals: null
              }
            }
          ]);
        }
        break;
      case SharedStateStatus.name:
        const sharedStateStatus = value as SharedStateStatus;
        if (sharedStateStatus.addWorkloadParameterSuccess) {
          this.store.dispatch(SharedActions.updateStatus({addWorkloadParameterSuccess: null}));
          this.createNewSizingRequest();
        }
        break;
    }
  }

  /**
   * method to validate form data
   * @param arg form elements value
   */
  performValidate(arg): void {
    validate(this.sizingFormData, {
      groups: ['step_'.concat(this.sizingFormData.step.toString())]
    }).then(errors => {
      // errors is an array of validation errors
      if (errors.length > 0) {
        console.log('validation failed. errors: ', errors);
        this.disableButton = true;
      } else {
        console.log('validation succeed');
        this.disableButton = false;
      }
      this.store.dispatch(
        SizingActions.setSizingData({ sizingForm: this.sizingFormData })
      );
      this.callOutErrors(errors, arg);
    });
  }

  /**
   * method t show error when invalid input
   * @param errors form error
   * @param arg form data
   */
  callOutErrors(errors, arg): void {
    if (arg === false) {
      return;
    }
    const e = errors.find(i => i.property === arg);
    const obj = {};
    obj[arg] = null;
    if (e) {
      const keys = Object.keys(e.constraints);
      obj[arg] = e.constraints[keys[0]];
    }
    this.errors = {
      ...this.errors,
      ...obj
    };
    console.log(this.errors);
  }

  addParametersToWorkload() {
    const selectedWorkloadId = this.state[WorkLoads.name].find(
      w => w.name === this.sizingFormData.workloadType
    ).id;
    this.store.dispatch(
      SharedActions.getParametersForWorkload({ workloadId: selectedWorkloadId })
    );
  }

  /**
   * method to create new sizing request
   */
  createNewSizingRequest() {
    this.store.dispatch(
      SizingActions.addNewRequest({
        formData: this.sizingFormData,
        workloads: this.state[WorkLoads.name]
      })
    );
  }

  /**
   * method to navigate
   * @param action user action for navigation
   */
  handleNavigation(action): void {
    switch (action) {
      case 'cancel':
        this.store.dispatch(SizingActions.resetSizingData());
        this.router.navigate(['/']);
        break;
      case 'back':
        this.sizingFormData.step -= 1;
        this.performValidate(false);
        break;
      case 'next':
        this.sizingFormData.step += 1;
        this.performValidate(false);
        break;
      case 'submit':
        this.addParametersToWorkload();
        break;
      default:
        break;
    }
  }
}

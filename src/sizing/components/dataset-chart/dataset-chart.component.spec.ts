import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataSetChartComponent } from './dataset-chart.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

xdescribe('DataSetChartComponent', () => {
  let component: DataSetChartComponent;
  let fixture: ComponentFixture<DataSetChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataSetChartComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataSetChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

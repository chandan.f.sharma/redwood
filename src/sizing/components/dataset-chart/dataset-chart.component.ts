import { Component, OnInit, Input } from '@angular/core';
import * as Highcharts from 'highcharts';
import { LocalChartOptionValues } from '@sizing-module/models/dataset-chart-values.model';
import { GlobalChartOptions } from '@sizing-module/const/chart-constants';

@Component({
  selector: 'app-dataset-chart',
  templateUrl: './dataset-chart.component.html',
  styleUrls: ['./dataset-chart.component.scss']
})
export class DataSetChartComponent implements OnInit {

  globalChartOptions: Highcharts.Options;

  datasetChart: Highcharts.Chart;

  /**
   * Intercepting received chart data and updating inside Global chart data
   */
  @Input() set chartData(localChartOptions: LocalChartOptionValues) {

    // Inserting X Axis label values
    const xAxis: Highcharts.XAxisOptions = this.globalChartOptions.xAxis as Highcharts.XAxisOptions;
    xAxis.categories = localChartOptions.categories;

    // Inserting numerical values for correspoding y axis
    this.globalChartOptions.series = localChartOptions.series as Highcharts.SeriesOptionsType[];
  }

  /**
   * Intialising our local chart data with Global chart data
   */
  constructor() {
    this.globalChartOptions = JSON.parse(JSON.stringify(GlobalChartOptions)) as Highcharts.Options;
  }

  /**
   * Instantiating Highcharts with Chart options
   */
  ngOnInit() {
    this.datasetChart = Highcharts.chart('container-dataset', this.globalChartOptions);
  }
}

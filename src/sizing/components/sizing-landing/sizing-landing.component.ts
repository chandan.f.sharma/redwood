import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { SharedStateStatus } from '../../../shared/models/state.model';
import { Tab } from '../../../shared/models/tab.model';
import { UtilitiesService } from 'src/app/services/utilities.service';
import * as SharedSelectors from '../../../shared/store/shared.selector';
import * as SizingSelectors from '../../store/sizing.selector';
import * as SharedActions from '../../../shared/store/shared.action';
import * as sizingAction from '../../store/sizing.action';
import { ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Parameter } from '@shared-module/models/parameter.model';
import { SizingRequest } from '@shared-module/models/sizing-request.model';
import { SizingWorkload } from '@shared-module/models/sizing-workload.model';
import { WorkLoads } from '@shared-module/models/workload.model';
import { SizingService } from '@sizing-module/service/sizing.service';
import { ModalService } from '@shared-module/services/modal.service';

@Component({
  selector: 'app-sizing-landing',
  templateUrl: './sizing-landing.component.html',
  styleUrls: ['./sizing-landing.component.scss']
})
export class SizingLandingComponent implements OnInit {
  state: object;

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  headerTabs: Tab[];

  typeTabs: Tab[];

  nameTags: Tab[];

  selectedWLType: string;

  selectedWLName: string;

  parameters: Parameter[];

  view: string;

  showModal: boolean;

  addWorkLoad: { modal: string; type: string; name: string };

  wName: string;

  errors: {};

  sizingRequest: SizingRequest;

  workLoads: WorkLoads[];

  workloadTypes: { key: number; value: string }[];

  constructor(
    private store: Store<any>,
    private service: SizingService,
    private utilitiesService: UtilitiesService,
    private modalService: ModalService,
    private route: ActivatedRoute
  ) {
    this.addWorkLoad = { modal: 'ADD_WORKLOAD', type: null, name: null };
    this.view = 'Capacity';
    this.headerTabs = [
      { id: 1, value: 'Capacity', icon: 'icon-data-center', selected: true },
      { id: 2, value: 'CloudOut', icon: 'icon-cloud-stroke' },
      { id: 3, value: 'EBSPricing', icon: 'icon-ebs-volume' }
    ];
  }

  ngOnInit() {
    this.errors = {};
    let selectedRequestId;
    this.route.params.pipe(takeUntil(this.ngUnsubscribe)).subscribe(params => {
      selectedRequestId = +params.id;
    });
    this.state = { callBack: this.storeCallBack.bind(this) };
    this.utilitiesService.listenToStateStore(
      this.store,
      [
        SizingSelectors.selectSizingRequest,
        SharedSelectors.selectWorkLoads,
        SharedSelectors.selectSharedStateStatus
      ],
      [{ id: selectedRequestId }],
      { state: this.state, subject: this.ngUnsubscribe }
    );
    this.store.dispatch(SharedActions.getAllSizingRequests());
    this.store.dispatch(SharedActions.getAllWorkloads());
  }

  /**
   * call back function to return updated store data
   * @param type data type
   * @param value data value
   */
  storeCallBack(type: string, value: any) {
    switch (type) {
      case SizingRequest.name:
        this.parameters = null;
        this.nameTags = null;
        this.typeTabs = null;
        this.sizingRequest = value;
        this.closeModal(this.addWorkLoad.modal);
        if (this.sizingRequest && this.sizingRequest.workloads.length) {
          this.prepareWorkloadTypes();
        }
        break;
      case WorkLoads.name:
        this.workLoads = value;
        this.workloadTypes = value.map(v => {
          return { key: v.id, value: v.name };
        });
        break;
      case SharedStateStatus.name:
        const sharedStateStatus = value as SharedStateStatus;
        if (sharedStateStatus.addWorkloadParameterSuccess) {
          this.store.dispatch(
            SharedActions.updateStatus({ addWorkloadParameterSuccess: null })
          );
          this.addWorkloadToRequest();
        }
        break;
    }
  }

  /**
   * method to store workload type
   */
  prepareWorkloadTypes() {
    this.typeTabs = [];
    this.sizingRequest.workloads.forEach(w => {
      const id = this.typeTabs.length + 1;
      const selected = id === 1 ? true : false;
      if (!this.typeTabs.find(t => t.value === w.type)) {
        this.typeTabs.push({ id, value: w.type, selected });
      }
    });
    const tab = this.typeTabs.find(t => t.selected);
    this.showWorkloadsOfType(tab);
  }

  /**
   * method to store workload
   * @param tab  workload type
   */
  showWorkloadsOfType(tab: Tab) {
    this.nameTags = [];
    this.selectedWLType = tab.value;
    this.sizingRequest.workloads.forEach(w => {
      const id = this.nameTags.length + 1;
      const selected = id === 1 ? true : false;
      if (w.type === this.selectedWLType) {
        this.nameTags.push({ id, value: w.name, selected });
      }
    });

    this.showParametersForWorkload(this.nameTags.find(t => t.selected));
  }

  /**
   * stores workload name
   * @param tag workload name
   */
  showParametersForWorkload(tag: Tab) {
    this.selectedWLName = tag.value;

    const wl: SizingWorkload = this.sizingRequest.workloads.find(
      w => w.type === this.selectedWLType && w.name === this.selectedWLName
    );
    this.parameters = wl.parameters;
    console.log('showParametersForWorkload ', this.parameters);
  }

  /**
   * update workload type
   * @param tab workload type
   */
  onHeaderTabChange(tab: Tab) {
    this.view = tab.value;
  }

  /**
   * method to open modal
   * @param modalId modal name
   */
  openModal(modalId: string): void {
    this.modalService.openModal(modalId);
  }

  /**
   * method to close modal
   * @param modalId modal name
   */
  closeModal(modalId: string): void {
    this.modalService.closeModal(modalId);
  }

  addWorkloadParametersToWorkload(): void {
    const workloadId = this.workloadTypes.find(
      w => w.value === this.addWorkLoad.type
    ).key;
    this.store.dispatch(SharedActions.getParametersForWorkload({ workloadId }));
  }

  /**
   * add workload
   */
  addWorkloadToRequest(): void {
    this.sizingRequest = this.service.addWorkloadToSizingRequest(
      this.sizingRequest,
      this.addWorkLoad.type,
      this.addWorkLoad.name,
      this.workLoads
    );
    this.store.dispatch(
      sizingAction.updateRequest({ request: this.sizingRequest })
    );
  }

  /**
   * remove workload
   * @param tag workload name
   */
  removeWorkloadFromRequest(tag: Tab) {
    const typeName = this.selectedWLType + '_' + tag.value;
    this.sizingRequest.workloads = this.sizingRequest.workloads.filter(w => {
      const tagName = w.type + '_' + w.name;
      return tagName !== typeName;
    });
    this.store.dispatch(
      sizingAction.updateRequest({ request: this.sizingRequest })
    );
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SizingLandingComponent } from './sizing-landing.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { sharedReducer } from '@shared-module/store/shared.reducer';
import { UtilitiesService } from '@app-module/services/utilities.service';
import { FakeUtilService, FakeModalService, RouterStub, FakeActivatedRoute, FakeSizingService } from 'src/mocks/angular-mock';
import { ModalService } from '@shared-module/services/modal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SizingService } from '@sizing-module/service/sizing.service';
import { plainToClass } from 'class-transformer';
import { SizingRequest } from '@shared-module/models/sizing-request.model';

describe('SizingLandingComponent', () => {
  let component: SizingLandingComponent;
  let fixture: ComponentFixture<SizingLandingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SizingLandingComponent ],
      imports: [
        StoreModule.forRoot(sharedReducer)
      ],
      providers: [
        {provide: UtilitiesService, useClass: FakeUtilService},
        {provide: SizingService, useClass: FakeSizingService},
        {provide: ModalService, useClass: FakeModalService},
       {provide: ActivatedRoute,  useClass: FakeActivatedRoute}],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SizingLandingComponent);
    component = fixture.componentInstance;
    component.addWorkLoad = { modal: 'ADD_WORKLOAD', type: null, name: null };
    component.view = 'Capacity';
    component.headerTabs = [
      { id: 1, value: 'Capacity', icon: 'icon-data-center', selected: true },
      { id: 2, value: 'CloudOut', icon: 'icon-cloud-stroke' },
      { id: 3, value: 'EBSPricing', icon: 'icon-ebs-volume' }
    ];
    fixture.detectChanges();
  });

  it('should create', () => {
    component.sizingRequest = sizingrequest;
    component.storeCallBack('SizingRequest', sizingrequest);
    expect(component).toBeTruthy();
  });

  it('should not update workloads when empty', () => {
    component.storeCallBack('SizingRequest', sizingrequests);
    expect(component).toBeTruthy();
  });

  it('should  update workloads', () => {
    component.typeTabs = [{id: 1, selected: false}];
    component.storeCallBack('SizingRequest', sizingrequest);
    expect(component).toBeTruthy();
  });

  it('should trigger callback with workloads details', () => {
    component.sizingRequest = sizingrequest;
    const spy = jest.spyOn(component, 'storeCallBack');
    component.storeCallBack('WorkLoads', sizingrequest.workloads);
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should  update header tab', () => {
    const tab = {id: 1, value: 'Capacity', selected: false};
    const spy = jest.spyOn(component, 'onHeaderTabChange');
    component.onHeaderTabChange(tab);
    expect(spy).toHaveBeenCalledTimes(1);
    expect(component.view).toEqual('Capacity');
  });

  it('should  create sizing request', () => {
    const spy = jest.spyOn(component, 'addWorkloadToRequest');
    component.addWorkloadToRequest();
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should  open modal', () => {
    const spy = jest.spyOn(component, 'openModal');
    component.openModal('1');
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should  remove workload', () => {
    component.sizingRequest = sizingrequest;
    const tab = {id: 1, value: 'Capacity', selected: false};
    const spy = jest.spyOn(component, 'removeWorkloadFromRequest');
    component.removeWorkloadFromRequest(tab);
    expect(spy).toHaveBeenCalledTimes(1);
  });
});

const param =  [{
  id: 1,
  key: '',
  type: true,
  order: 1,
  groupName: undefined,
  enabled: true,
  hidden: true,
  required: true,
  heading: '',
  title: '',
  interaction: undefined,
  hint: '',
  default: '',
  placeholder: '',
  class: '',
  options: [],
  actions: {'': [{prop: '', target: '', value: true}]},
  value: '',
  validations: [],
  parameters: [],
  beginAtNewLine: true,
  isDropDown: true,
  dropdownOptions: [],
  isInputField: true,
  regExes: [],
  errors: []
}];

const sizingrequest = {
     id: 1,
     companyName: 'Azure',
      sizingRequestName: 'New Test Request',
      industry: 'Data Solutions',
      location: 'England',
      oppurtunity: 'adds',
      dataGrowthRate: 3,
      storageType: 'Yes',
      workloads: [{
        type: '',
        name: '',
        parameters: param
      }]
};
const sizingrequests = {
  id: 1,
  companyName: 'Azure',
   sizingRequestName: 'New Test Request',
   industry: 'Data Solutions',
   location: 'England',
   oppurtunity: 'adds',
   dataGrowthRate: 3,
   storageType: 'Yes',
   workloads: []
};

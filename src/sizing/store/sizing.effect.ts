import { Injectable, OnDestroy } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  map,
  takeUntil,
  mergeMap,
  catchError,
  exhaustMap,
  switchMap
} from 'rxjs/operators';
import * as SizingAction from './sizing.action';
import * as SharedAction from '../../shared/store/shared.action';
import { SizingService } from '../service/sizing.service';
import { Subject, of } from 'rxjs';
import { plainToClass, classToPlain } from 'class-transformer';
import { SizingRequest } from '@shared-module/models/sizing-request.model';
import { Router } from '@angular/router';

@Injectable()
export class SizingEffect implements OnDestroy {
  constructor(
    private actions$: Actions,
    private sizingService: SizingService
  ) {}

  ngUnSubscribe: Subject<void> = new Subject<void>();

  setFormOptionsEffect = createEffect(() =>
    this.actions$.pipe(
      ofType(SizingAction.getFormOptions),
      mergeMap(() =>
        this.sizingService.getFormOptions().pipe(
          map(responseList => {
            return SizingAction.setFormOptions({
              companies: responseList[0],
              industries: responseList[1],
              locations: responseList[2]
            });
          }),
          catchError(() => of(undefined))
        )
      )
    )
  );

  createNewRequestEffect = createEffect(() =>
    this.actions$.pipe(
      ofType(SizingAction.addNewRequest),
      exhaustMap(payload => {
        let request = plainToClass(
          SizingRequest,
          classToPlain(payload.formData),
          { excludeExtraneousValues: true }
        );
        request = this.sizingService.addWorkloadToSizingRequest(
          request,
          payload.formData.workloadType,
          payload.formData.workloadName,
          payload.workloads
        );

        return this.sizingService.createNewSizingRequest(request).pipe(
          switchMap(resp => [
            SizingAction.addRequestSuccess({addRequestSuccess: resp.id}),
            SharedAction.addNewRequest(resp)
          ]),
          catchError((error: Error) => of(SizingAction.addRequestFailure({addRequestFailure: error.message})))
        );
      })
    )
  );


  updateRequestEffect = createEffect(() =>
    this.actions$.pipe(
      ofType(SizingAction.updateRequest),
      exhaustMap(payload => {
        return this.sizingService.updateSizingRequest(payload.request).pipe(
          switchMap(resp => [
            SharedAction.updateRequestSuccess({sizingRequest: resp}),
            SizingAction.updateRequestSuccess()
          ]),
          catchError((error: Error) => of(SizingAction.updateRequestFailure({updateRequestFailure: error.message})))
        );
      })
    )
  );

  ngOnDestroy() {}
}

import { createReducer, on } from '@ngrx/store';
import * as sizingAction from './sizing.action';
import { plainToClass } from 'class-transformer';
import { SizingState } from '../models/state.model';

export const initialState: SizingState = plainToClass(SizingState, {});

const reducer = createReducer(
  initialState,
  on(sizingAction.setFormOptions, (state, payload) => ({
    ...state,
    ...payload
  })),
  on(sizingAction.setSizingData, (state, payload) => ({
    ...state,
    ...payload
  })),
  on(sizingAction.resetSizingData, state => ({
    ...state,
    sizingForm: initialState.sizingForm
  })),
  on(sizingAction.addRequestSuccess, (state, payload) => ({
    ...state,
    addRequestSuccess: payload.addRequestSuccess,
    addRequestFailure: null
  })),
  on(sizingAction.updateRequestSuccess, state => ({
    ...state,
    updateRequestSuccess: true,
    updateRequestFailure: null
  })),
  on(sizingAction.updateStatus, (state, payload) => ({
    ...state,
    ...payload
  }))
);

export function sizingReducer(state, action) {
  return reducer(state, action);
}

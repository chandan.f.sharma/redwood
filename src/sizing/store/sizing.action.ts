import { createAction, props, Action } from '@ngrx/store';
import {
  Company,
  Industry,
  Location,
  SizingForm
} from '../models/sizing.form.model';
import { WorkLoad } from '@shared-module/models/workload.model';
import { SizingRequest } from '@shared-module/models/sizing-request.model';

export const setSizingData = createAction(
  '[SizingModule] SetSizingData',
  props<{ sizingForm: SizingForm }>()
);

export const setFormOptions = createAction(
  '[SizingModule] SetCompanies',
  props<{
    companies: Company[];
    industries: Industry[];
    locations: Location[];
  }>()
);
export const getSizingData = createAction('[SizingModule] GetSizingData');
export const resetSizingData = createAction('[SizingModule] ResetSizingData');
export const getFormOptions = createAction('[SizingModule] GetFormOptions');

export const addNewRequest = createAction(
  '[SizingModule] AddNewRequest',
  props<{ formData: SizingForm; workloads: WorkLoad[] }>()
);

export const addRequestSuccess = createAction(
  '[SizingModule] addRequestSuccess',
  props<{ addRequestSuccess: number }>()
);

export const addRequestFailure = createAction(
  '[SizingModule] addRequestFailure',
  props<{ addRequestFailure: string }>()
);

export const updateRequest = createAction(
  '[SizingModule] updateRequest',
  props<{ request: SizingRequest }>()
);

export const updateRequestSuccess = createAction(
  '[SizingModule] updateRequestSuccess'
);

export const updateRequestFailure = createAction(
  '[SizingModule] updateRequestFailure',
  props<{ updateRequestFailure: string }>()
);

export const updateStatus = createAction(
  '[SizingModule] updateStatus',
  props<{
    addRequestSuccess?: number;
    updateRequestSuccess?: boolean;
    addRequestFailure?: string;
    updateRequestFailure?: string;
  }>()
);

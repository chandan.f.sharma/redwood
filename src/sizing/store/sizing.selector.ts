import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  Company,
  Industry,
  Location,
  SizingForm
} from '../models/sizing.form.model';
import { SizingState, SizingStatus } from '../models/state.model';
import { SharedState } from '@shared-module/models/state.model';
import { SizingRequest } from '@shared-module/models/sizing-request.model';

export const selectSharedFeature = createFeatureSelector('sharedFeature');
export const selectSizingFeature = createFeatureSelector('sizingFeature');

export const selectCompanies = createSelector(
  selectSizingFeature,
  (state: { sizing: SizingState }) => state.sizing.companies
);
selectCompanies.prototype.class = Company;

export const selectIndustries = createSelector(
  selectSizingFeature,
  (state: { sizing: SizingState }) => state.sizing.industries
);
selectIndustries.prototype.class = Industry;

export const selectLocations = createSelector(
  selectSizingFeature,
  (state: { sizing: SizingState }) => state.sizing.locations
);
selectLocations.prototype.class = Location;

export const selectSizingForm = createSelector(
  selectSizingFeature,
  (state: { sizing: SizingState }) => state.sizing.sizingForm
);
selectSizingForm.prototype.class = SizingForm;

export const selectSizingRequest = createSelector(
  selectSharedFeature,
  (state: { shared: SharedState }, props) => {
    return state.shared.sizingRequests.find(r => r.id === props.id);
  }
);
selectSizingRequest.prototype.class = SizingRequest;
selectSizingRequest.prototype.hasProps = true;

export const selectSizingStatus = createSelector(
  selectSizingFeature,
  (state: { sizing: SizingState }) => {
    return {
      addRequestFailure: state.sizing.addRequestFailure,
      addRequestSuccess: state.sizing.addRequestSuccess,
      updateRequestFailure: state.sizing.updateRequestFailure,
      updateRequestSuccess: state.sizing.updateRequestSuccess
    };
  }
);
selectSizingStatus.prototype.class = SizingStatus;

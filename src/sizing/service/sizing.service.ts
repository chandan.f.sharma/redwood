import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { SizingRequest } from '@shared-module/models/sizing-request.model';
import { WorkLoad } from '@shared-module/models/workload.model';
import { plainToClass } from 'class-transformer';
import { SizingWorkload } from '@shared-module/models/sizing-workload.model';

@Injectable({
  providedIn: 'root'
})
export class SizingService {
  constructor(private http: HttpClient) {}

  getFormOptions(): Observable<any[]> {
    const response1 = this.http.get('companies');
    const response2 = this.http.get('industries');
    const response3 = this.http.get('locations');
    return forkJoin([response1, response2, response3]);
  }

  createNewSizingRequest(props: SizingRequest): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post('requests', props, {
      headers
    });
  }

  updateSizingRequest(props: SizingRequest): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.put('requests/' + props.id, props, {
      headers
    });
  }

  addWorkloadToSizingRequest(
    request: SizingRequest,
    workloadType: string,
    workloadName: string,
    workloads: WorkLoad[]
  ): SizingRequest {
    const sizingWorkLoad = plainToClass(SizingWorkload, {
      name: workloadName,
      type: workloadType
    });
    const workload = workloads.find(w => w.name === workloadType);
    const parameters = workload ? [...workload.parameters] : [];
    sizingWorkLoad.parameters = parameters;
    request.workloads = [...request.workloads, sizingWorkLoad];
    return request;
  }
}

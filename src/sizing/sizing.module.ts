import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SizingLandingComponent } from './components/sizing-landing/sizing-landing.component';
import { RouterModule, Routes } from '@angular/router';
import { SizingModalComponent } from './components/sizing-modal/sizing-modal.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { sizingReducer } from './store/sizing.reducer';
import { SizingEffect } from './store/sizing.effect';
import { SharedModule } from '../shared/shared.module';
import { SizingParametersComponent } from './components/sizing-parameters/sizing-parameters.component';
import { DataSetChartComponent } from './components/dataset-chart/dataset-chart.component';
import { DatasetChartContainerComponent } from './components/dataset-chart-container/dataset-chart-container.component';
import { ReqCapacityChartContainerComponent } from './components/req-capacity-chart-container/req-capacity-chart-container.component';
import { RequiredCapacityChartComponent } from './components/required-capacity-chart/required-capacity-chart.component';

const routes: Routes = [
  { path: '', redirectTo: 'new' },
  { path: 'new', component: SizingModalComponent },
  { path: ':id', component: SizingLandingComponent }
];

@NgModule({
  declarations: [
    SizingLandingComponent,
    SizingModalComponent,
    SizingParametersComponent,
    DataSetChartComponent,
    DatasetChartContainerComponent,
    ReqCapacityChartContainerComponent,
    RequiredCapacityChartComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    RouterModule.forChild(routes),
    SharedModule,
    StoreModule.forFeature('sizingFeature', { sizing: sizingReducer }),
    EffectsModule.forFeature([SizingEffect])
  ]
})
export class SizingModule {}

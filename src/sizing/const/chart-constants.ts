export const CONSTANTS = {
  xAxisLabels: {
    UsableCapacity: 'Usable Capacity',
    ReserveCapacity: 'Reserve Capacity',
    TotalUsableCapacity: 'Total Usable Capacity',
    ReplicationSeedingTime: 'Replication Seeding Time',
    ReplicationofIncrementals: 'Replication of Incrementals'
  },

  LegendName: {
    NAS: 'NAS',
    VM: 'VM',
    UsableCapacity: 'Usable Capacity',
    ReserveSpace: 'Reserve Space',
    TotalCapacity: 'Total Capacity'
  }
};

export const GlobalChartOptions = {
  chart: {
    type: 'column',
    zoomType: 'xy'
  },
  credits: {
    enabled: false
  },
  title: {
    text: ''
  },
  xAxis: {
    labels: {
      autoRotation: false,
      formatter() {
        switch (this.value) {
          case CONSTANTS.xAxisLabels.UsableCapacity:
          case CONSTANTS.xAxisLabels.ReserveCapacity:
          case CONSTANTS.xAxisLabels.TotalUsableCapacity:
            return this.value + '\n (Space in TB)';
            break;

          case CONSTANTS.xAxisLabels.ReplicationSeedingTime:
          case CONSTANTS.xAxisLabels.ReplicationofIncrementals:
            return this.value + '\n (Bandwidth in mb/s)';
            break;

          default:
            return this.value;
            break;
        }
      }
    },

    categories: []
  },
  yAxis: {
    gridLineWidth: 0,
    lineWidth: 1,
    title: ''
  },
  tooltip: {
    headerFormat: `<span style='font-size:10px'>{point.key}</span><table>`,
    pointFormat: `<tr><td style='color:{series.color};padding:0'>{series.name}: </td>
    <td style='padding:0'><b>{point.y}</b></td></tr>`,
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },

  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },

  legend: {
    align: 'center',
    verticalAlign: 'bottom',
    floating: false,
    borderColor: '#CCC',
    layout: 'horizontal',
    symbolRadius: 0,
    y: 0,
    borderWidth: 0,
    backgroundColor: '#FFFFFF',
    shadow: false
  },
  series: []
};

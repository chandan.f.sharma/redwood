import { Observable, of as observableOf, Subject } from 'rxjs';
import { NavigationExtras, UrlSegment, Params } from '@angular/router';
import { StateCallbackOrValues } from '@app-module/services/utilities.service';
import { SizingRequest } from '@shared-module/models/sizing-request.model';
import { WorkLoad } from '@shared-module/models/workload.model';


export class RouterStub {
    public url = '';
    public routerState?: object;
    public events = observableOf(Event);
    navigate(commands: any[], extras?: NavigationExtras) { }
  }

export class FakeActivatedRoute {
    /** An observable of the URL segments matched by this route. */
    url: Observable<UrlSegment[]>;
    /** An observable of the matrix parameters scoped to this route. */
    params: Observable<Params> = observableOf({params: { id: '23'}});
    /** An observable of the query parameters shared by all the routes. */
    queryParams: Observable<Params>;
  }

export class MockStore {
    public dispatch(obj) {
      console.log('dispatching from the mock store!');
    }

    public select(obj) {
      console.log('selecting from the mock store!');

      return observableOf({});
    }

    public pipe(obj) {
      console.log('pipe from the mock store!');

      return observableOf(Boolean);
    }
  }

export class FakeModalService {
    openModal(id: string) {
    }

    closeModal(id: string) {
    }

    register(id: string) {
    }
  }

export class FakeUtilService {
    arrayOfSize(n: number): Array<number> {
      return [];
    }
    listenToStateStore(
      store,
      arrSelector,
      arrProps,
      objState: { state: StateCallbackOrValues; subject: Subject<void> }
    ): void {
    }
  }

export class FakeSizingService {

  getFormOptions(): Observable<any[]> {
    return observableOf([]);
  }

  createNewSizingRequest(props: SizingRequest): Observable<any> {
    return observableOf([]);
  }

  addWorkloadToSizingRequest(
    request: SizingRequest,
    workloadType: string,
    workloadName: string,
    workloads: WorkLoad[]
  ): SizingRequest {
    return request;
  }

  }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';


const routes: Routes = [
  {path: 'Dashboard', component: DashboardComponent},
  {path: '', redirectTo: 'Dashboard', pathMatch: 'full'},
  {path: 'Dashboard/UserManagement', loadChildren: () => import('../user/user.module').then(m => m.UserModule)},
  {path: 'Dashboard/WorkloadManagement', loadChildren: () => import('../workload/workload.module').then(m => m.WorkLoadModule)},
  {path: 'Dashboard/SizingDetail', loadChildren: () => import('../sizing/sizing.module').then(m => m.SizingModule)},
  // Auxilary Paths
  {path: 'CreateNewRequest', loadChildren: () => import('../sizing/sizing.module').then(m => m.SizingModule), outlet: 'forModals'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


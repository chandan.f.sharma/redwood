import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { Store, select } from '@ngrx/store';
import { takeUntil } from 'rxjs/operators';
import { plainToClass } from 'class-transformer';

@Injectable({
  providedIn: 'root'
})
export class UtilitiesService {
  constructor() { }

  /**
   * This function will produce an array of numbers of the size provided.
   * @param n : Number of which size the array is required.
   */
  arrayOfSize(n: number): Array<number> {
    let count = 0;
    const arr = [];
    while (count < n) {
      count++;
      arr.push(count);
    }
    return arr;
  }

  /**
   * This function is used to reduce the code duplication for binding the components with state store.
   * It is the responsibility of caller to check if order of selectors and props is same.
   * @param store : NgRx store
   * @param arrSelector : Array of selectors of what is required from store
   * @param arrProps : Array of props if any selecor requires them
   * @param objState : An object which would hold values fetched from store
   */

  listenToStateStore(
    store,
    arrSelector,
    arrProps,
    objState: { state: StateCallbackOrValues; subject: Subject<void> }
  ): void {
    let index = 0;
    arrSelector.forEach(selector => {
      const props = selector.prototype.hasProps ? arrProps[index++] : null;
      store
        .pipe(select(selector, props))
        .pipe(takeUntil(objState.subject))
        .subscribe(state => {
          objState.state[selector.prototype.class.name] = plainToClass(
            selector.prototype.class,
            state
          );
          if (objState.state.callBack) {
            objState.state.callBack(
              selector.prototype.class.name,
              plainToClass(selector.prototype.class, state)
            );
          }
        });
    });
  }

  /**
   * method to sort array in ascending order
   * @param collection : An array which needed to be sorted
   * @param sortBy: A key of an object from collection
   */
  sortBy(collection: any[], sortBy: any): any[] {
    collection.sort((a, b) => {
      return a[sortBy] - b[sortBy];
    });
    return collection;
  }
}

export interface StateCallbackOrValues {
  [key: string]: any;
}


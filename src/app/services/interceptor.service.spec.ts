import { TestBed } from '@angular/core/testing';

import { InterceptorService } from './interceptor.service';
import { MockStore } from "src/mocks/angular-mock";
import { Store } from '@ngrx/store';


describe('InterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [{ provide: Store, useClass: MockStore },
    InterceptorService]
  }));

  it('should be created', () => {
    const service: InterceptorService = TestBed.get(InterceptorService);

    expect(service).toBeTruthy();
  });
});

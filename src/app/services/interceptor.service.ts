import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse,
  HttpHandler,
  HttpEvent
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { SharedState } from '@shared-module/models/state.model';
import { showLoader, hideLoader } from '@shared-module/store/shared.action';
import { environment } from '../../environments/environment';

@Injectable()
export class InterceptorService implements HttpInterceptor {

  constructor(private store: Store<SharedState>) {
    console.log('InterceptorService Service Initialized');
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.store.dispatch(showLoader());
    const clonedReq = request.clone({url: environment.baseApiUrl + request.url});
    return next.handle(clonedReq).pipe(
      tap((e: HttpEvent<any>) => {
        if (e instanceof HttpResponse) {
          this.store.dispatch(hideLoader());
        }
      }),
      catchError(response => {
        if (response instanceof HttpErrorResponse) {
          this.store.dispatch(hideLoader());
        }
        return throwError(response);
      })
    );
  }
}

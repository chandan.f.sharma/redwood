import { Component } from '@angular/core';
import {
  Router,
  NavigationStart,
  NavigationEnd,
  ActivatedRoute
} from '@angular/router';
import { Store } from '@ngrx/store';
import { SharedState } from '@shared-module/models/state.model';
import {
  showLoader,
  hideLoader,
  updateBreadCrumbs
} from '@shared-module/store/shared.action';
import { BreadCrumb } from '@shared-module/models/bread-crumbs.model';
import { pairwise } from 'rxjs/operators';
import { Location } from '@angular/common';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private router: Router,
    private store: Store<SharedState>,
    private location: Location
  ) {
    this.router.events.pipe(pairwise()).subscribe(e => {
      // console.log(e)
      if (e[0] instanceof NavigationStart) {
        this.store.dispatch(showLoader());
      }

      if (e[0] instanceof NavigationEnd) {
        console.log('navend   ', e);
        this.store.dispatch(hideLoader());
        if (e[0].urlAfterRedirects.indexOf('forModals') !== -1) {
          this.location.replaceState('Dashboard');
          return;
        }
        const urlAfterRedirects = e[0].urlAfterRedirects;
        const arrPaths = urlAfterRedirects.split('/');
        arrPaths.shift();
        // console.log(arrPaths);
        let breadCrumbs: BreadCrumb[] = arrPaths.map(p => ({
          path: p,
          name: p
        }));
        breadCrumbs = breadCrumbs.filter(b => b.path.length > 1);
        if (breadCrumbs) {
          this.store.dispatch(updateBreadCrumbs({ breadcrumbs: breadCrumbs }));
        }
      }
    });
  }
}

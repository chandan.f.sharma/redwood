import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { BreadCrumbsComponent } from "./bread-crumbs.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { MockStore, FakeUtilService, RouterStub } from "src/mocks/angular-mock";
import { Store } from "@ngrx/store";
import { UtilitiesService } from "@app-module/services/utilities.service";
import { Router } from "@angular/router";

describe("BreadCrumbsComponent", () => {
  let component: BreadCrumbsComponent;
  let fixture: ComponentFixture<BreadCrumbsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BreadCrumbsComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: Store, useClass: MockStore },
        { provide: UtilitiesService, useClass: FakeUtilService },
        { provide: Router, useClass: RouterStub }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreadCrumbsComponent);
    component = fixture.componentInstance;
    component.breadCrumbs = [{ path: "path", name: "testName" }];
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});

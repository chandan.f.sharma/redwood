import { Component, OnInit } from '@angular/core';
import { UtilitiesService } from 'src/app/services/utilities.service';
import { Store } from '@ngrx/store';
import { SharedState } from '@shared-module/models/state.model';
import { Subject } from 'rxjs';
import * as SharedSelectors from '../../../shared/store/shared.selector';
import { BreadCrumb } from '@shared-module/models/bread-crumbs.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-bread-crumbs',
  templateUrl: './bread-crumbs.component.html',
  styleUrls: ['./bread-crumbs.component.scss']
})
export class BreadCrumbsComponent implements OnInit {
  state: object;

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  breadCrumbs: BreadCrumb[];

  constructor(
    private utilitiesService: UtilitiesService,
    private sharedStore: Store<SharedState>,
    private router: Router
  ) {}

  ngOnInit() {
    this.state = {
      callBack: this.storeCallBack.bind(this)
    };
    this.utilitiesService.listenToStateStore(
      this.sharedStore,
      [SharedSelectors.selectBreadCrumbs],
      [],
      { state: this.state, subject: this.ngUnsubscribe }
    );
  }

  storeCallBack(type: string, value: BreadCrumb[]) {
    switch (type) {
      case BreadCrumb.name:
        this.breadCrumbs = value ? value.map(b => {
          b.name = b.name.split(/(?=[A-Z])/).join(' ');
          return b;
        }) : [];
        break;
    }
  }

  navigateToLink(index: number) {
    const arrBreadCrumbs = this.breadCrumbs.slice(0, index + 1);
    const arrPaths = arrBreadCrumbs.map(b => b.path);
    const path = arrPaths.join('/');
    this.router.navigate([path]);


  }
}

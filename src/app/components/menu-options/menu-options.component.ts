import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-menu-options',
  templateUrl: './menu-options.component.html',
  styleUrls: ['./menu-options.component.scss']
})
export class MenuOptionsComponent implements OnInit {

  arrOptions: {path: string, label: string}[];

  state: string;

  constructor() { }

  ngOnInit() {
    this.arrOptions = [
      {path: 'Dashboard', label: 'Dashboard'},
      {path: 'Dashboard/WorkloadManagement', label: 'Workload Management'},
      {path: '/logout', label: 'Logout'},
    ];

    this.state = null;
  }

  open(e: Event) {
    this.state = 'open';
    e.stopImmediatePropagation();
  }

  @HostListener('document:click', ['$event'])
  close() {
    this.state = null;
  }

}

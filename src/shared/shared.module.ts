import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoaderComponent } from './components/loader/loader.component';
import { StepperComponent } from './components/stepper/stepper.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { sharedReducer } from './store/shared.reducer';
import { SharedEffect } from './store/shared.effects';
import { SharedService } from './services/shared.service';
import { ParameterComponent } from './components/parameter/parameter.component';
import { FormsModule } from '@angular/forms';
import { AutoSuggestComponent } from './components/auto-suggest/auto-suggest.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { TabLayoutComponent } from './components/tab-layout/tab-layout.component';
import { TagLayoutComponent } from './components/tag-layout/tag-layout.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { CommonModalComponent } from './components/common-modal/common-modal.component';
import { ModalService } from './services/modal.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    StoreModule.forFeature('sharedFeature', {shared: sharedReducer}),
    EffectsModule.forFeature([SharedEffect])
  ],
  declarations: [
    LoaderComponent,
    StepperComponent,
    ParameterComponent,
    AutoSuggestComponent,
    TabLayoutComponent,
    TagLayoutComponent,
    DropdownComponent,
    CommonModalComponent
  ],
  providers: [
    SharedService,
    ModalService,
    HttpClient
  ],
  exports: [
    // Shared Components
    LoaderComponent,
    StepperComponent,
    ParameterComponent,
    AutoSuggestComponent,
    TabLayoutComponent,
    TagLayoutComponent,
    DropdownComponent,
    CommonModalComponent,
    // Shared Modules

    FormsModule
  ]
})
export class SharedModule { }

export class Tab {
    id: number;
    value?: string;
    icon?: string;
    selected?: boolean;
   }


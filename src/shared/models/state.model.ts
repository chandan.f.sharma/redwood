import { Expose, Type } from 'class-transformer';
import { WorkLoad } from './workload.model';
import { SizingRequest } from './sizing-request.model';
import { BreadCrumb } from './bread-crumbs.model';

export class SharedStateStatus {
  @Expose()
  getWorkloadSuccess: boolean;

  @Expose()
  getWorkloadFailure: string;

  @Expose()
  addWorkloadParameterSuccess: boolean;

  @Expose()
  addWorkloadParameterFailure: string;

  @Expose()
  getSizingRequestSuccess: boolean;

  @Expose()
  getSizingRequestFailure: string;
}

export class SharedState extends SharedStateStatus {
  @Expose()
  @Type(() => WorkLoad)
  workloads: WorkLoad[];

  @Expose()
  @Type(() => SizingRequest)
  sizingRequests: SizingRequest[];

  @Expose()
  @Type(() => BreadCrumb)
  breadcrumbs: BreadCrumb[];

  @Expose()
  loading: boolean;
}

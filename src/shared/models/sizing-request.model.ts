import { Expose, Transform, Type } from 'class-transformer';
import { SizingWorkload } from './sizing-workload.model';

export class SizingRequest {
    @Expose()
    id: number;

    @Expose()
    @Transform(value => value || '', { toClassOnly: true })
    companyName: string;

    @Expose()
    @Transform(value => value || '', {toClassOnly: true})
    sizingRequestName: string;

    @Expose()
    @Transform(value => value || '', {toClassOnly: true})
    industry: string;

    @Expose()
    @Transform(value => value || '', {toClassOnly: true})
    location: string;

    @Expose()
    @Transform(value => value || '', {toClassOnly: true})
    oppurtunity: string;

    @Expose()
    @Transform(value => value || '', {toClassOnly: true})
    dataGrowthRate: number;

    @Expose()
    @Transform(value => value || '', {toClassOnly: true})
    storageType: string;

    @Expose()
    @Transform(value => value || [], {toClassOnly: true})
    @Type(() => SizingWorkload)
    workloads: SizingWorkload[];
}

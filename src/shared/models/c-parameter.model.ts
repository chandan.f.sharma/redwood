import { Expose, Transform, Type } from 'class-transformer';


export class CParameter {
    @Expose()
    key: string;

    @Expose()
    value: string;

    @Expose()
    type: number;
   }


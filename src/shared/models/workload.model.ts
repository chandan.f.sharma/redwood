import {Expose, Transform, Type } from 'class-transformer';
import { Parameter } from './parameter.model';

export class WorkLoad {
    @Expose()
    id: number;

    @Expose()
    name: string;

    @Expose()
    @Type(() => Parameter)
    @Transform(value => value || [], {
        toClassOnly: true
    })
    parameters: Parameter[];
}

export class WorkLoads extends WorkLoad {}

import { Expose } from 'class-transformer';

export class Option {
  @Expose()
  key: number;

  @Expose()
  value: string;
}

import { Expose, Transform, Type } from 'class-transformer';

export class ParameterAction {
  @Expose()
  target?: string;

  @Expose()
  prop?: string;

  @Expose()
  value?: boolean;
}

@Expose()
export class Action {

  [key: string]: ParameterAction[];
}

@Expose()
export class Validation {
  @Expose()
  regex: string;

  @Expose()
  error: string;
}

export class Parameter {
  @Expose()
  id: number; /* Non visual */

  @Expose()
  key: string; /* Unique parameter name for a workload*  [field_key]*/

  @Expose()
  type: boolean; /* Group or No Group  [parameter_type]*/

  @Expose()
  groupName: string; /* [parameter_type]*/

  @Expose()
  @Transform(value => (typeof value !== 'boolean' ? true : value), {
    toClassOnly: true
  })
  enabled: boolean; /*[is_readonly]*/

  @Expose()
  @Transform(value => (typeof value !== 'boolean' ? true : value), {
    toClassOnly: true
  })
  hidden: boolean; /* [is_hidden] */

  @Expose()
  @Transform(value => (typeof value !== 'boolean' ? true : value), {
    toClassOnly: true
  })
  required: boolean; /* [is_required] */

  @Expose()
  @Transform(value => value || '', { toClassOnly: true })
  heading: string; /* [heading_label] */

  @Expose()
  @Transform(value => value || '', { toClassOnly: true })
  title: string; /* [label] */

  @Expose()
  interaction: number; /* [field_type] */

  @Expose()
  @Transform(value => value || '', { toClassOnly: true })
  hint: string; /* [comment] */

  @Expose()
  @Transform(value => value || '', { toClassOnly: true })
  default: string; /* [default_value] */

  @Expose()
  @Transform(value => value || '', { toClassOnly: true })
  placeholder: string; /* [placeholder] */

  @Expose()
  @Transform(value => value || '', { toClassOnly: true })
  class: string; /* [class] */

  @Expose()
  @Transform(value => value || [], { toClassOnly: true })
  options: string[]; /* [dropdown_value] */

  @Expose()
  @Type(() => Action)
  actions: Action;

  @Expose()
  value: string;

  @Expose()
  @Type(() => Validation)
  @Transform(value => value || [], { toClassOnly: true })
  validations: Validation[]; /* [validation_type] */

  @Expose()
  order: number; /* [order_by]*/

  @Expose()
  @Type(() => Parameter)
  @Transform(value => value || [], { toClassOnly: true })
  parameters: Parameter[];

  @Expose()
  get beginAtNewLine() {
    return this.class && this.class.includes('newline');
  }

  @Expose()
  get isDropDown() {
    return this.interaction === 0;
  }

  @Expose()
  get dropdownOptions() {
    return this.options.map((o, i) => {
      return { key: i, value: o };
    });
  }

  @Expose()
  get isInputField() {
    return this.interaction === 1;
  }

  @Expose()
  get regExes() {
    const regs = this.validations ? this.validations.map(v => new RegExp(v.regex)) : [];
    return regs;
  }

  @Expose()
  get errors() {
    const errors = this.validations ? this.validations.map(v => new RegExp(v.error)) : [];
    return errors;
  }
}

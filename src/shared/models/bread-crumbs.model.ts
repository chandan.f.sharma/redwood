import { Expose } from 'class-transformer';

export class BreadCrumb {
    @Expose()
    path: string;

    @Expose()
    name: string;
}

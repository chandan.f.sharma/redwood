import { Expose, Type } from 'class-transformer';
import { Parameter } from './parameter.model';

export class SizingWorkload {
    @Expose()
    type: string;

    @Expose()
    name: string;

    @Expose()
    @Type(() => Parameter)
    parameters: Parameter[];
}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Option } from '../../models/option.model';


@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})

export class DropdownComponent implements OnInit {

  localValue: string;

  @Input() get value(): string {
    return this.localValue;
  }

  set value(val) {
    this.localValue = val;
    this.valueChange.emit(this.localValue);
  }

  @Input() placeholder: string;

  @Input() options: Option[];

  @Output() valueChange: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  // handleChange() {
  //   this.valueChange.emit(this.value);
  // }

}

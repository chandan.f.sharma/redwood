import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Tab } from '@shared-module/models/tab.model';

@Component({
  selector: 'app-tag-layout',
  templateUrl: './tag-layout.component.html',
  styleUrls: ['./tag-layout.component.scss']
})
export class TagLayoutComponent {
  @Input() tags: Tab[];

  @Output() tagChange: EventEmitter<Tab> = new EventEmitter();

  @Output() remove: EventEmitter<Tab> = new EventEmitter();

  constructor() {}

  setSelected(tab: Tab) {
    this.tags = this.tags.map(t => {
      t.selected = false;
      if (t.id === tab.id) {
         t.selected = true;
      }
      return t;
    });
    this.tagChange.emit(tab);
  }

  cancel(e: Event, tag: Tab) {
    this.remove.emit(tag);
    e.stopImmediatePropagation();
  }
}

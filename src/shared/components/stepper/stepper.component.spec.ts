import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { StepperComponent } from "./stepper.component";
import { UtilitiesService } from "@app-module/services/utilities.service";
import { FakeUtilService } from "src/mocks/angular-mock";

describe("StepperComponent", () => {
  let component: StepperComponent;
  let fixture: ComponentFixture<StepperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StepperComponent],
      providers: [{ provide: UtilitiesService, useClass: FakeUtilService }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});

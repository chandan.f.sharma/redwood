import { Component, OnInit, Input } from '@angular/core';
import { UtilitiesService } from 'src/app/services/utilities.service';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit {

  @Input() steps: number;
  @Input() currentStep: number;

  arrSteps: number[];

  constructor(private utilitiesService: UtilitiesService) { }

  ngOnInit() {
    this.arrSteps = this.utilitiesService.arrayOfSize(this.steps);
  }


}

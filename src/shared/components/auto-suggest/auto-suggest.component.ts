import { Component, OnInit, HostListener, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-auto-suggest',
  templateUrl: './auto-suggest.component.html',
  styleUrls: ['./auto-suggest.component.scss']
})
export class AutoSuggestComponent implements OnInit {
  @Input() key: string;
  @Input() list: string[];
  @Input() placeholder: string;
  @Input() selected: string;
  @Output() textChange = new EventEmitter<string>();

  value: string;

  filteredList: string[];

  constructor() { }

  ngOnInit() {
    this.filteredList = [];
    console.log('this.selected  ', this.selected);
    setTimeout(() => {
      this.value = this.selected;
    }, 0);

  }

  showSuggestion(event): void {
    this.filteredList = [];
    if (this.value !== '') {
      this.list.forEach(element => {
        if (element.toLocaleLowerCase().startsWith(this.value.toLowerCase())) {
          this.filteredList.push(element);
        }
      });
    }
    // this.checkTypedValue();
    this.textChange.emit(this.value);
  }

  setSelected(value: string) {
    this.value = value;
    this.textChange.emit(this.value);
  }

  handleFieldClick(e: Event) {
    e.stopImmediatePropagation();
  }

  @HostListener('document: click', ['$event.target'])
  cancel() {
    this.filteredList = [];
  }

}

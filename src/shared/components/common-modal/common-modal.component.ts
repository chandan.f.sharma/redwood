import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'app-common-modal',
  templateUrl: './common-modal.component.html',
  styleUrls: ['./common-modal.component.scss']
})
export class CommonModalComponent implements OnInit {

  @Input() id: string;

  @Input() size: string;

  @Output() modalClose: EventEmitter<void> = new EventEmitter<void>();

  state: boolean;

  handle: EventEmitter<string>;

  constructor(private modalService: ModalService) {

  }

  ngOnInit() {
    this.handle = this.modalService.register(this.id);
    this.state = false;
    this.handle.subscribe(data => {
      switch (data) {
        case 'OPEN':
          this.open();
          break;

        case 'CLOSE':
          this.close();
          break;
      }
    });
  }
  open() {
    this.state = true;
  }

  close() {
    this.state = false;
    this.modalClose.emit();
  }
}

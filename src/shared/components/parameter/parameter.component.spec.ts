import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParameterComponent } from './parameter.component';
import { FormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Parameter } from '@shared-module/models/parameter.model';

describe('ParameterComponent', () => {
  let component: ParameterComponent;
  let fixture: ComponentFixture<ParameterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParameterComponent ],
      imports: [ FormsModule ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParameterComponent);
    component = fixture.componentInstance;
    component.parameter = { value : 'value', default : 'default', heading : 'heading', title : 'title', hint : 'hint'} as Parameter;
    component.value = 'Value';
    component.error = 'Error';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

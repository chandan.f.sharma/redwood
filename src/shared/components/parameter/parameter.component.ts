import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Parameter } from '../../models/parameter.model';

@Component({
  selector: 'app-parameter',
  templateUrl: './parameter.component.html',
  styleUrls: ['./parameter.component.scss']
})
export class ParameterComponent implements OnInit {
  @Input() parameter: Parameter;
  @Output() answer = new EventEmitter<Parameter>();

  value: string;

  error: string;

  constructor() {}

  ngOnInit() {
    this.error = '';
    this.value = this.parameter.value
      ? this.parameter.value
      : this.parameter.default
      ? this.parameter.default
      : null;
  }

  validate() {
    this.error = '';
    if (!this.value || !this.value.trim().length) {
      if (this.parameter.required) {
        this.error = 'This field is required';
      }
      return;
    }

    this.parameter.regExes.some((r, i) => {
      if (!r.test(this.value)) {
        this.error += this.parameter.errors[i];
        return true;
      }
      return false;
    });
  }

  handleChange(val: string): void {
    this.parameter.value = this.value;
    this.answer.emit(this.parameter);
  }
}

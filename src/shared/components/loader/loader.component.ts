import { Component, OnInit } from '@angular/core';
import { SharedState } from '@shared-module/models/state.model';
import { Store, select } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { selectLoading } from '../../store/shared.selector';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  sharedState: Observable<SharedState>;

  loading: boolean;

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private store: Store<SharedState>) { }

  ngOnInit() {
    this.store.pipe(select(selectLoading)).pipe(takeUntil(this.ngUnsubscribe)).subscribe(data => {
      this.loading = data;
    });
  }

}

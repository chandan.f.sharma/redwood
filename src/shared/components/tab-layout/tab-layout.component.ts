import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Tab } from '@shared-module/models/tab.model';

@Component({
  selector: 'app-tab-layout',
  templateUrl: './tab-layout.component.html',
  styleUrls: ['./tab-layout.component.scss']
})
export class TabLayoutComponent {
  @Input() tabs: Tab[];

  @Input() fontClass: string;

  @Output() tabChange: EventEmitter<Tab> = new EventEmitter();

  constructor() { }

  setSelected(tab: Tab) {
    this.tabs = this.tabs.map(t => {
      t.selected = false;
      if (t.id === tab.id) {
        t.selected = true;
      }
      return t;
    });
    this.tabChange.emit(tab);
  }

}

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements OnInit {
  @Input() icon: string;

  collapsed: boolean;

  constructor() { }

  ngOnInit() {
    this.collapsed = false;
  }

}

import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  map,
  mergeMap,
  catchError,
  exhaustMap,
  switchMap
} from 'rxjs/operators';
import { of } from 'rxjs';
import * as SharedActions from './shared.action';
import { SharedService } from '../services/shared.service';
import { UtilitiesService } from '@app-module/services/utilities.service';
import { Parameter } from '@shared-module/models/parameter.model';

@Injectable()
export class SharedEffect {
  constructor(
    private actions$: Actions,
    private utilService: UtilitiesService,
    private sharedService: SharedService
  ) {}

  getAllWorkloadsEffect = createEffect(() =>
    this.actions$.pipe(
      ofType(SharedActions.getAllWorkloads),
      mergeMap(() =>
        this.sharedService.getAllWorkloads().pipe(
          switchMap(workloads => [
            SharedActions.setAllWorkloads({
              workloads
            }),
            SharedActions.getWorkloadSuccess()
          ]),
          catchError((error: Error) =>
            of(
              SharedActions.getWorkloadFailure({
                getWorkloadFailure: error.message
              })
            )
          )
        )
      )
    )
  );

  getAllRequestsEffect = createEffect(() =>
    this.actions$.pipe(
      ofType(SharedActions.getAllSizingRequests),
      mergeMap(() =>
        this.sharedService.getAllRequests().pipe(
          switchMap(requests => [
            SharedActions.setAllSizingRequests({
              sizingRequests: requests
            }),
            SharedActions.getSizingRequestSuccess()
          ]),
          catchError((error: Error) =>
            of(
              SharedActions.getSizingRequestFailure({
                getSizingRequestFailure: error.message
              })
            )
          )
        )
      )
    )
  );

  getAllParamEffect = createEffect(() =>
    this.actions$.pipe(
      ofType(SharedActions.getParametersForWorkload),
      exhaustMap(payload => {
        return this.sharedService
          .getParametersForWorkload(payload.workloadId)
          .pipe(
            map(parameters => {
              const groupTypeParameters: Parameter[] = parameters.filter(
                p => p.type
              );
              let individualParameters: Parameter[] = parameters.filter(
                p => !p.type
              );
              groupTypeParameters.forEach(group => {
                group.parameters = individualParameters.filter(
                  p => p.groupName === group.key
                );
                group.parameters = this.utilService.sortBy(
                  group.parameters,
                  'order'
                );
              });
              individualParameters = individualParameters.filter(
                p => !p.groupName || p.groupName.length === 0
              );
              const workloadParameters = this.utilService.sortBy(
                [...groupTypeParameters, ...individualParameters],
                'order'
              );
              return workloadParameters;
            }),
            switchMap(workloadParameters => [
              SharedActions.setWorkloadParameters({
                workloadId: payload.workloadId,
                parameters: workloadParameters
              }),
              SharedActions.addWorkloadParameterSuccess()
            ]),
            catchError((error: Error) =>
              of(
                SharedActions.addWorkloadParameterFailure({
                  addWorkloadParameterFailure: error.message
                })
              )
            )
          );
      })
    )
  );
}

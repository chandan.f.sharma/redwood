import { createReducer, on } from "@ngrx/store";
import * as SharedActions from "./shared.action";
import { SharedState } from "../models/state.model";
import { plainToClass } from "class-transformer";

export const initialState: SharedState = plainToClass(SharedState, {
  workloads: [],
  sizingRequests: []
});

const reducer = createReducer(
  initialState,
  on(SharedActions.showLoader, state => ({
    ...state,
    loading: true
  })),
  on(SharedActions.hideLoader, state => ({
    ...state,
    loading: false
  })),
  on(SharedActions.setAllWorkloads, (state, payload) => ({
    ...state,
    ...payload
  })),
  on(SharedActions.setAllSizingRequests, (state, payload) => ({
    ...state,
    ...payload
  })),
  on(SharedActions.addNewRequest, (state, payload) => {
    const nState = { ...state };
    nState.sizingRequests.push(payload);
    return nState;
  }),
  on(SharedActions.updateRequestSuccess, (state, payload) => {
    const nState = { ...state };
    nState.sizingRequests = nState.sizingRequests.filter(
      r => r.id !== payload.sizingRequest.id
    );
    nState.sizingRequests = [...nState.sizingRequests, payload.sizingRequest];
    return nState;
  }),
  on(SharedActions.addNewWorkload, (state, payload) => {
    const nState = { ...state };
    nState.workloads = [...nState.workloads, payload.newWorkload];
    return nState;
  }),
  on(SharedActions.updateWorkload, (state, payload) => {
    const nState = { ...state };
    nState.workloads = nState.workloads.filter(
      wl => wl.id !== payload.workload.id
    );
    nState.workloads = [...nState.workloads, payload.workload];
    return nState;
  }),
  on(SharedActions.updateBreadCrumbs, (state, payload) => ({
    ...state,
    breadcrumbs: [...payload.breadcrumbs]
  })),
  on(SharedActions.setWorkloadParameters, (state, payload) => {
    const nState = { ...state };
    const workload = nState.workloads.find(w => w.id === payload.workloadId);
    const workloads = nState.workloads.filter(w => w.id !== payload.workloadId);
    workload.parameters = payload.parameters;
    nState.workloads = [{ ...workload }, ...workloads];
    return nState;
  }),
  on(SharedActions.getWorkloadSuccess, state => ({
    ...state,
    getWorkloadSuccess: true,
    getWorkloadFailure: null
  })),
  on(SharedActions.getSizingRequestSuccess, state => ({
    ...state,
    getSizingRequestSuccess: true,
    getSizingRequestFailure: null
  })),
  on(SharedActions.addWorkloadParameterSuccess, state => ({
    ...state,
    addWorkloadParameterSuccess: true,
    addWorkloadParameterFailure: null
  })),
  on(SharedActions.getWorkloadFailure, (state, payload) => ({
    ...state,
    ...payload
  })),
  on(SharedActions.getSizingRequestFailure, (state, payload) => ({
    ...state,
    ...payload
  })),
  on(SharedActions.addWorkloadParameterFailure, (state, payload) => ({
    ...state,
    ...payload
  })),
  on(SharedActions.updateStatus, (state, payload) => ({
    ...state,
    ...payload
  }))
);

export function sharedReducer(state, action) {
  return reducer(state, action);
}

import { createAction, props } from '@ngrx/store';
import { WorkLoad } from '../models/workload.model';
import { SizingRequest } from '../models/sizing-request.model';
import { BreadCrumb } from '../models/bread-crumbs.model';
import { Parameter } from '@shared-module/models/parameter.model';

export const showLoader = createAction('[SharedModule] ShowLoader');

export const hideLoader = createAction('[SharedModule] HideLoader');

export const getAllWorkloads = createAction('[SharedModule] GetALLWorkloads');

export const getWorkloadSuccess = createAction(
  '[SharedModule] getWorkloadSuccess'
);

export const getWorkloadFailure = createAction(
  '[SharedModule] getWorkloadFailure',
  props<{ getWorkloadFailure: string }>()
);

export const setAllWorkloads = createAction(
  '[SharedModule] SetALLWorkloads',
  props<{ workloads: WorkLoad[] }>()
);

export const getParametersForWorkload = createAction(
  '[SharedModule] getParametersForWorkload',
  props<{ workloadId: number }>()
);

export const addWorkloadParameterSuccess = createAction(
  '[SharedModule] addWorkloadParameterSuccess'
);

export const addWorkloadParameterFailure = createAction(
  '[SharedModule] addWorkloadParameterFailure',
  props<{ addWorkloadParameterFailure: string }>()
);

export const setWorkloadParameters = createAction(
  '[SharedModule] setWorkloadParameters',
  props<{ workloadId: number; parameters: Parameter[] }>()
);

export const getAllSizingRequests = createAction(
  '[SharedModule] GetALLRequests'
);

export const getSizingRequestSuccess = createAction(
  '[SharedModule] getSizingRequestSuccess'
);

export const getSizingRequestFailure = createAction(
  '[SharedModule] getSizingRequestFailure',
  props<{ getSizingRequestFailure: string }>()
);

export const setAllSizingRequests = createAction(
  '[SharedModule] SetAllRequests',
  props<{ sizingRequests: SizingRequest[] }>()
);

export const addNewRequest = createAction(
  '[SharedModule] AddNewRequest',
  props<SizingRequest>()
);

export const updateRequestSuccess = createAction(
  '[SharedModule] UpdateRequestSuccess',
  props<{ sizingRequest: SizingRequest }>()
);

export const addNewWorkload = createAction(
  '[SharedModule] AddNewWorkload',
  props<{ newWorkload: WorkLoad }>()
);

export const createNewWorkload = createAction(
  '[SharedModule] CreateNewWorkload',
  props<{ newWorkload: WorkLoad }>()
);

export const getWorkloadById = createAction(
  '[SharedModule] GetWorkloadById',
  props<{ id: number }>()
);

export const updateWorkload = createAction(
  '[SharedModule] UpdateWorkload',
  props<{ workload: WorkLoad }>()
);

export const updateBreadCrumbs = createAction(
  '[SharedModule] UpdateBreadCrumbs',
  props<{ breadcrumbs: BreadCrumb[] }>()
);

export const updateStatus = createAction(
  '[SharedModule] updateStatus',
  props<{
    getWorkloadSuccess?: boolean;
    getWorkloadFailure?: string;
    addWorkloadParameterSuccess?: boolean;
    addWorkloadParameterFailure?: string;
    getSizingRequestSuccess?: boolean;
    getSizingRequestFailure?: string;

  }>()
);

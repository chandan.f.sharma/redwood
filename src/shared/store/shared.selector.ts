import { createFeatureSelector, createSelector } from '@ngrx/store';
import { WorkLoads, WorkLoad } from '../models/workload.model';
import { SharedState, SharedStateStatus } from '../models/state.model';
import { BreadCrumb } from '../models/bread-crumbs.model';

export const selectSharedFeature = createFeatureSelector('sharedFeature');

export const selectLoading = createSelector(
  selectSharedFeature,
  (state: {shared: SharedState}) => state.shared.loading
);
selectLoading.prototype.class = class Loading {};

export const selectWorkLoads = createSelector(
  selectSharedFeature,
  (state: {shared: SharedState}) => state.shared.workloads
);
selectWorkLoads.prototype.class = WorkLoads;

export const selectWorkLoadWithId = createSelector(
  selectSharedFeature,
  (state: {shared: SharedState}, props) => {
    return state.shared.workloads.find(w => w.id === props.id);
  }
);
selectWorkLoadWithId.prototype.class = WorkLoad;
selectWorkLoadWithId.prototype.hasProps = true;

export const selectBreadCrumbs = createSelector(
  selectSharedFeature,
  (state: {shared: SharedState}) => state.shared.breadcrumbs
);
selectBreadCrumbs.prototype.class = BreadCrumb;

export const selectSharedStateStatus = createSelector(
  selectSharedFeature,
  (state: {shared: SharedState}) => ({
    addWorkloadParameterSuccess: state.shared.addWorkloadParameterSuccess,
    addWorkloadParameterFailure: state.shared.addWorkloadParameterFailure,
    getWorkloadSuccess: state.shared.getWorkloadSuccess,
    getWorkloadFailure: state.shared.getWorkloadFailure,
    getSizingRequestSuccess: state.shared.getSizingRequestSuccess,
    getSizingRequestFailure: state.shared.getSizingRequestFailure
  })
);
selectSharedStateStatus.prototype.class = SharedStateStatus;

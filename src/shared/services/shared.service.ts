import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Parameter } from '@shared-module/models/parameter.model';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(private http: HttpClient ) { }

  getAllWorkloads(): Observable<any> {
    return this.http.get('workloads');
  }

  getAllRequests(): Observable<any> {
    return this.http.get('requests');
  }

  /**
   * method to get workload param
   * @param id: workload id
   */
  getParametersForWorkload(workloadId: number): Observable<any> {
    return this.http.get('workloadparameters?workloadId=' + workloadId);
  }
}

import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  handlesMap: { [key: string]: EventEmitter<string> };

  constructor() {
    this.handlesMap = {};
  }

  openModal(id: string) {
    try {
      this.handlesMap[id].emit('OPEN');
    } catch (error) {
      console.error('Could not open modal', error);
    }
  }

  closeModal(id: string) {
    try {
      this.handlesMap[id].emit('CLOSE');
    } catch (error) {}
  }

  register(id: string) {
    // console.log('register ', id);
    if (!this.handlesMap[id]) {
      this.handlesMap[id] = new EventEmitter<string>();
    }
    // console.log('register ', id, this.handlesMap[id]);
    return this.handlesMap[id];
  }
}
